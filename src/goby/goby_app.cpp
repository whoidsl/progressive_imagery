/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goby/middleware/multi-thread-application.h"
#include "config.pb.h"

#include <boost/units/io.hpp>

#include "progressive_imagery/manager.h"
#include "progressive_imagery/progressive_imagery.pb.h"

#include "progressive_imagery/goby/groups.h"
#include "goby/middleware/gobyd/groups.h"
#include "goby/middleware/protobuf/intervehicle_status.pb.h"

using goby::glog;
using namespace goby::common::logger;


namespace dsl
{
    namespace progressive_imagery
    {
        using AppBase = ::goby::MultiThreadApplication<dsl::progressive_imagery::protobuf::ProgressiveImageAppConfig>;
        class GobyProgressiveImageApp : public AppBase
        {
        public:
            GobyProgressiveImageApp();
            
        private:
            // BOTH
            void loop() override;
            ManagerBase* create_manager();

            dsl::progressive_imagery::ProgressiveCompressorConfiguration::ProgressionOrder progression_order()
                {
                    switch(cfg().sender().compressor_defaults().progression_order())
                    {
                        case protobuf::CompressorSettings::LAYER_RESOLUTION_COMPONENT_PRECINCT: 
                            return ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT;
                        case protobuf::CompressorSettings::RESOLUTION_LAYER_COMPONENT_PRECINCT:
                            return ProgressiveCompressorConfiguration::ProgressionOrder::RESOLUTION_LAYER_COMPONENT_PRECINCT;
                    }
                }

            bool send_data();

            // SENDER
            void publish_transfer_progress();

            // RECEIVER
            void publish_received_status();

            
        private:
            std::unique_ptr<ManagerBase> manager_;
            int last_tx_queue_size_ {-1};

            // SENDER: images last acked
            std::vector<dsl::protobuf::ImageRxStatus> last_rx_status_; 

            goby::time::MicroTime next_resend_time_ { goby::time::now() };
        };
    }
}

namespace dpi = dsl::progressive_imagery;

int main(int argc, char* argv[])
{ return goby::run<dpi::GobyProgressiveImageApp>(argc, argv); }

dpi::GobyProgressiveImageApp::GobyProgressiveImageApp() :
    AppBase(10*boost::units::si::hertz),
    manager_(create_manager())
{
    
    switch(cfg().role())
    {
        case protobuf::ProgressiveImageAppConfig::SENDER:
        {
            publish_transfer_progress();

            intervehicle().subscribe_dynamic<dsl::protobuf::J2KQueueUpdateAndAck>(
                [this](const dsl::protobuf::J2KQueueUpdateAndAck& ack)
                {
                    glog.is_debug1() && glog << "Received ack" << ack.ShortDebugString() << std::endl;
                    auto sender_manager = dynamic_cast<ManagerSender*>(manager_.get());
                    auto last_updated_images = sender_manager->post_queue_update_and_ack(ack);
                    
                    for(auto image_id : last_updated_images)
                    {
                        dsl::protobuf::ImageRxStatus image_rx_status;
                        image_rx_status.set_image_id(image_id);
                        image_rx_status.set_fraction_received(sender_manager->level2_image_meta(image_id).received_fraction());
                        last_rx_status_.push_back(image_rx_status);
                    }
                    
                    
                    // if we've just gotten an ack, and our queue is empty, send more data
                    next_resend_time_ = goby::time::now();
                    if(cfg().role() == protobuf::ProgressiveImageAppConfig::SENDER)
                        publish_transfer_progress();
                }
                );
            break;
        }

        case protobuf::ProgressiveImageAppConfig::RECEIVER:
        {
            intervehicle().subscribe_dynamic<dsl::protobuf::J2KMainHeader>(
                [this](const dsl::protobuf::J2KMainHeader& header)
                {
                    auto receiver_manager = dynamic_cast<ManagerReceiver*>(manager_.get());
                    receiver_manager->post_header(header);

                    next_resend_time_ = goby::time::now();
                }
                );

            intervehicle().subscribe_dynamic<dsl::protobuf::J2KPacketFragment>(
                [this](const dsl::protobuf::J2KPacketFragment& fragment)
                {
                    auto receiver_manager = dynamic_cast<ManagerReceiver*>(manager_.get());
                    auto updated_ids = receiver_manager->post_fragment(fragment);
                    
                    for(auto image_id : updated_ids)
                    {
                        dsl::protobuf::UpdatedImageEvent updated_image;
                        updated_image.set_image_id(image_id);
                        updated_image.set_image_path(receiver_manager->id_to_path(image_id).native());
                        interprocess().publish<dpi::groups::updated_image>(updated_image);
                    }
                    
                    publish_received_status();

                    next_resend_time_ = goby::time::now();
                }
                );


            break;
        }
        
    }

    interprocess().subscribe<goby::groups::intervehicle_outbound,
                             goby::protobuf::InterVehicleStatus >(
                                 [this](const goby::protobuf::InterVehicleStatus& status)
                                 {
                                     last_tx_queue_size_ = status.tx_queue_size();
                                 });

    interprocess().subscribe<dsl::progressive_imagery::groups::queue_action,
                             dsl::protobuf::QueueAction>(
                                 [this](const dsl::protobuf::QueueAction& pb_actions)
                                 {
                                     std::vector<dsl::progressive_imagery::QueueAction> actions;
                                     for(auto pb_action : pb_actions.queue_action())
                                         actions.push_back( { static_cast<int>(pb_action.image_id()), pb_action.fraction_goal() } );

                                     manager_->post_queue_update(actions);
                                 }
                                 );
}


dpi::ManagerBase* dpi::GobyProgressiveImageApp::create_manager()
{
    auto image_type = dpi::ImageType::TIFF;
    switch(cfg().image_type())
    {
        case dpi::protobuf::ProgressiveImageAppConfig::TIFF:
            image_type = dpi::ImageType::TIFF;
            break;
            
        case dpi::protobuf::ProgressiveImageAppConfig::PNG:
            image_type = dpi::ImageType::PNG;
            break;

        case dpi::protobuf::ProgressiveImageAppConfig::PPM:
            image_type = dpi::ImageType::PPM;
            break;

    }

    dpi::ManagerCommonConfiguration common_cfg { image_type, cfg().img_dir() };

    switch(cfg().app().glog_config().tty_verbosity())
    {
        case goby::common::protobuf::GLogConfig::DEBUG3: // pass through intentional
        case goby::common::protobuf::GLogConfig::DEBUG2: // pass through intentional
        case goby::common::protobuf::GLogConfig::DEBUG1: 
            common_cfg.info_callback = [](const std::string& s) { glog.is(DEBUG2) && glog << s << std::endl; };
            // pass through intentional
            
        case goby::common::protobuf::GLogConfig::WARN: // pass through intentional
        case goby::common::protobuf::GLogConfig::VERBOSE:
            common_cfg.error_callback = [](const std::string& s) { glog.is(WARN) && glog << s << std::endl; };
            common_cfg.warning_callback = [](const std::string& s) { glog.is(WARN) && glog << s << std::endl; };            
            break;
            
        case goby::common::protobuf::GLogConfig::QUIET: break;
            
    }
    
    switch(cfg().role())
    {
        case protobuf::ProgressiveImageAppConfig::SENDER:
        {
            auto encode_callback = [this](const dsl::protobuf::J2KMainHeader& header, int dccl_encoded_bytes)
                {
                    dsl::protobuf::EncodeReport enc_report;
                    *enc_report.mutable_encoded_header() = header;
                    enc_report.set_dccl_encoded_bytes(dccl_encoded_bytes);
                    interprocess().publish<dpi::groups::encode_report>(enc_report);
                };
            
            return new ManagerSender({
                    common_cfg,
                        cfg().sender().image_filename_regex(),
                        { // preprocessor_defaults
                            "", "", // in_file, out_file - overwriiten by Manager
                                cfg().sender().preprocessor_defaults().max_width(),
                                cfg().sender().preprocessor_defaults().max_height(),
                                cfg().sender().preprocessor_defaults().make_grayscale()
                                },
                        { // compressor_defaults
                            { }, 0, "", // common, image_id, in_file - overwritten by Manager
                                     progression_order(),
                                     cfg().sender().compressor_defaults().number_of_resolutions(),
                                     cfg().sender().compressor_defaults().number_of_layers(),
                                     cfg().sender().compressor_defaults().max_approx_bytes(),
                                     cfg().sender().compressor_defaults().min_approx_bytes()
                                     },
                            cfg().sender().send_existing_images(),
                            cfg().sender().preprocess_subfolder(),
                            cfg().sender().pass1_fraction(),
                            encode_callback
                                } );
            break;
        }
        
        case protobuf::ProgressiveImageAppConfig::RECEIVER:
            return new ManagerReceiver( {
                    common_cfg,
                    {// decompressor_defaults
                        { },
                        cfg().receiver().decompressor_defaults().filename_prefix(),
                            cfg().receiver().decompressor_defaults().rename_partial()
                            }
                });
            break;    
    }
}


void dpi::GobyProgressiveImageApp::loop()
{
    try
    {
        if(cfg().role() == protobuf::ProgressiveImageAppConfig::SENDER)
        {
            if(manager_->check_for_new_images() > 0)
                publish_transfer_progress(); 
        }
    }
    catch(const dpi::Exception& e)
    {
        glog.is(WARN) && glog << "Exception while checking for new images: " << e.what() << std::endl;
    }

    auto now = goby::time::now();
    // if our queue is empty, and we've waited the allotted time, resend (presumably) unacked data
    if(last_tx_queue_size_ == 0 && now > next_resend_time_)
    {
        if(send_data())
            next_resend_time_ += goby::time::MicroTime(cfg().resend_delay_sec()*boost::units::si::seconds);

        glog.is_debug2() && glog << "next resend @ " << next_resend_time_ << std::endl;
    }
}


void dpi::GobyProgressiveImageApp::publish_transfer_progress()
{
    auto* sender_manager = dynamic_cast<ManagerSender*>(manager_.get());

    auto queue_it = sender_manager->level2_actions().begin();
    dsl::protobuf::TransferProgress progress;
    for(int i = 0, n = std::min<int>(cfg().sender().transfer_progress().number_of_next_actions(), sender_manager->level2_actions().size()); i < n; ++i)
    {
        auto& tx_action = *progress.add_next_actions();
        tx_action.set_image_id(queue_it->image_it_->compressor().image_id());
        tx_action.set_fraction_goal(queue_it->action_.fraction_goal);
        ++queue_it;
    }

    unsigned entirely_unsent_image = 0;
    for(const auto& image_meta : sender_manager->level2_image_queue())
    {
        if(image_meta.no_fragments_sent())
            ++entirely_unsent_image;
    }

    for(auto& s : last_rx_status_)
        *progress.add_rx_status_last_ack() = s;

    last_rx_status_.clear();

    progress.set_entirely_unsent_images(entirely_unsent_image);
    progress.set_sent_but_unacked_fragments(sender_manager->level1_queue().size());

    
    interprocess().publish<dpi::groups::transfer_progress>(progress);
    
}

bool dpi::GobyProgressiveImageApp::send_data()
{
    manager_->clear_staging_queue();

    bool data_sent = false;
    
    // not used by Goby driver
    std::vector<unsigned char> compressed;

    glog.is_debug2() && glog << "Getting data" << std::endl;
    
    std::vector<std::shared_ptr<google::protobuf::Message>> msgs = manager_->provide_compressed_data(compressed, cfg().num_bytes_per_cycle());

    if(compressed.size() != 0)
        glog.is_debug1() && glog << "sending " << compressed.size() << " bytes over " << msgs.size() << " messages."  << std::endl;
    
    for(auto& msg : msgs)
    {
        auto msg_type = msg->GetDescriptor()->name();
        if(msg_type == "J2KMainHeader")
        {
            const auto& typed_msg = dynamic_cast<const dsl::protobuf::J2KMainHeader&>(*msg);
            intervehicle().publish<dpi::groups::image_fragments>(typed_msg);
            data_sent = true;
        }
        else if(msg_type == "J2KPacketFragment")
        {
            const auto& typed_msg = dynamic_cast<const dsl::protobuf::J2KPacketFragment&>(*msg);
            intervehicle().publish<dpi::groups::image_fragments>(typed_msg);
            data_sent = true;
        }
        else if(msg_type == "J2KQueueUpdateAndAck")
        {

            const auto& typed_msg = dynamic_cast<const dsl::protobuf::J2KQueueUpdateAndAck&>(*msg);
            intervehicle().publish<dpi::groups::ack_and_queue_update>(typed_msg);
            data_sent = true;
        }
        else
        {
            glog.is(WARN) && glog << "Unexpected protobuf type returned from Manager: " << msg_type << std::endl;
        }

    }
    
    if(cfg().role() == protobuf::ProgressiveImageAppConfig::SENDER)
        publish_transfer_progress();

    next_resend_time_ = goby::time::now();
    
    return data_sent;
}


void dpi::GobyProgressiveImageApp::publish_received_status()
{
    dsl::protobuf::ReceivedStatus status;

    auto receiver_manager = dynamic_cast<ManagerReceiver*>(manager_.get());

    status.set_received_images(receiver_manager->rx_images().size());
    
    auto rx_it = receiver_manager->rx_images().begin();
    for(int i = 0, n = std::min<int>(cfg().receiver().received_status().number_of_images_reported(), receiver_manager->rx_images().size()); i < n; ++i)
    {
        auto& image_status = *status.add_image_status();
        image_status.set_image_id(rx_it->second.decompressor.image_id());
        image_status.set_fraction_received(rx_it->second.fraction_available);
        ++rx_it;
    }

    interprocess().publish<dpi::groups::received_status>(status);    
}
