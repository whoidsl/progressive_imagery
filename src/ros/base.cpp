/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "progressive_imagery/ros/base.h"


namespace dpi = dsl::progressive_imagery;

dpi::ros::NodeBase::NodeBase() :
    node_handle_("~"),
    data_server_(node_handle_.advertiseService("outgoing_data", &NodeBase::handle_data_request, this)),
    rx_data_sub_(node_handle_.subscribe("incoming_data", 10, &NodeBase::handle_incoming_data, this)),
    acks_sub_(node_handle_.subscribe("acks_expected", 1, &NodeBase::handle_acks_expected, this)),
    queue_update_sub_(node_handle_.subscribe("queue_update", 10, &NodeBase::handle_queue_update, this))
{

}

void dpi::ros::NodeBase::run(int argc, char** argv)
{
    ::ros::Rate loop_rate(10);
    while(::ros::ok())
    {
        loop();
        ::ros::spinOnce();
        loop_rate.sleep();
    }
}

void dpi::ros::NodeBase::handle_acks_expected(const ::ds_acomms_msgs::AcksExpected& msg)
{
  // TODO: Ideally, this would publish a log message regarding how many
  // chunks in the queue weren't acked. This would require adding
  // an appropriate accessor into the ManagerBase/ManagerSender to report
  // length of level0 queue.
  manager().clear_staging_queue();
}

void dpi::ros::NodeBase::handle_incoming_data(const ::ds_acomms_msgs::ModemData& msg)
{
    ROS_DEBUG_STREAM("Received data " << msg.payload.size() << "B, data hash: " << boost::hash<decltype(msg.payload)>()(msg.payload));

    try
    {
        auto updated_ids = manager().post_compressed_data(msg.payload);
        data_received_event(updated_ids);
    }
    catch(std::exception& e)
    {
        ROS_ERROR_STREAM("Exception while posting data: " << e.what());
    }
}

bool dpi::ros::NodeBase::handle_data_request(::ds_acomms_msgs::ModemDataRequest::Request& req,
					     ::ds_acomms_msgs::ModemDataRequest::Response& res)
{
    ROS_DEBUG_STREAM("Received data request: " << req);
    // TODO(LEL): Move this logic into separate callback function (acks_expected)
    //    if(req.reset_unacknowledged) {
    //        manager().clear_staging_queue();
    //    }

    try
    {
        manager().provide_compressed_data(res.data, req.bytes_requested);
    }
    catch(std::exception& e)
    {
        ROS_ERROR_STREAM("Exception while requesting data: " << e.what());
        return false;
    }
    //res.packet.stamp = ::ros::Time::now();

    ROS_DEBUG_STREAM("Sending response of " << res.data.size() << "B, data hash: " << boost::hash<decltype(res.data)>()(res.data));

    data_request_event();

    return true;
}

void dpi::ros::NodeBase::handle_queue_update(const ::progressive_imagery::QueueAction& ros_actions)
{
    std::vector<dpi::QueueAction> actions;
    for(auto ros_action : ros_actions.queue_action)
        actions.push_back( { static_cast<int>(ros_action.image_id), ros_action.fraction_goal } );

    ROS_DEBUG_STREAM("Queue Action: " << ros_actions);

    manager().post_queue_update(actions);
}
