/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "progressive_imagery/ros/base.h"

#include "progressive_imagery/ReceivedStatus.h"
#include "progressive_imagery/UpdatedImageEvent.h"

#include <iostream>

namespace dsl
{
    namespace progressive_imagery
    {
        namespace ros
        {
            struct ReceiverConfiguration
            {
                struct ReceivedStatusConfiguration
                {
                    int number_of_images_reported { 1000 };
                };
                
                ReceivedStatusConfiguration received_status;   
            };

            class Receiver : public NodeBase
            {
            public:
                Receiver();
                void loop() override;
                
            private:
                void data_received_event(const std::set<int>& updated_ids) override;
                
                ManagerReceiverConfiguration create_manager_configuration();
                ros::ReceiverConfiguration create_receiver_configuration();
                void publish_received_status();

                ManagerBase& manager() override
                { return receiver_manager_; }

            private:
                ManagerReceiver receiver_manager_;
                ros::ReceiverConfiguration receiver_cfg_;
                ::ros::Publisher status_pub_;
                ::ros::Publisher updated_image_pub_;
            };
        }
    }
}

namespace dpi = dsl::progressive_imagery;


int main(int argc, char** argv)
{
    ::ros::init(argc, argv, "progressive_imagery_receiver");   

    dpi::ros::Receiver receiver;
    receiver.run(argc, argv);
}

dpi::ros::Receiver::Receiver() :
    receiver_manager_(create_manager_configuration()),
    receiver_cfg_(create_receiver_configuration()),
    status_pub_(node_handle().advertise<::progressive_imagery::ReceivedStatus>("received_status", 10)),
    updated_image_pub_(node_handle().advertise<::progressive_imagery::UpdatedImageEvent>("updated_image_event", 10))
{       
}

void dpi::ros::Receiver::loop()
{
}
                          
void dpi::ros::Receiver::data_received_event(const std::set<int>& updated_ids)
{
    for(auto image_id : updated_ids)
    {
        ::progressive_imagery::UpdatedImageEvent updated_image;
        updated_image.image_id = image_id;
        updated_image.image_path = receiver_manager_.id_to_path(image_id).native();
        updated_image_pub_.publish(updated_image);
    }

    publish_received_status();
}



void dpi::ros::Receiver::publish_received_status()
{
    ::progressive_imagery::ReceivedStatus status;

    status.received_images = receiver_manager_.rx_images().size();
    
    auto rx_it = receiver_manager_.rx_images().begin();
    for(int i = 0, n = std::min<int>(receiver_cfg_.received_status.number_of_images_reported, receiver_manager_.rx_images().size()); i < n; ++i)
    {
        status.image_status.emplace_back();
        auto& image_status = status.image_status.back();
        image_status.image_id = rx_it->second.decompressor.image_id();
        image_status.fraction_received = rx_it->second.fraction_available;
        ++rx_it;
    }

    
    status_pub_.publish(status);
}


#define SET_CONFIG(_NAME)                                               \
    node_handle().getParam(#_NAME, cfg._NAME);                           \
    ROS_INFO_STREAM("Parameter " #_NAME " set to " << cfg._NAME);

#define SET_CONFIG_CHILD(_CHILD, _NAME)                                 \
    node_handle().getParam(#_CHILD "/" #_NAME, cfg._CHILD._NAME);        \
    ROS_INFO_STREAM("Parameter " #_CHILD "." #_NAME " set to " << cfg._CHILD._NAME);


dpi::ManagerReceiverConfiguration dpi::ros::Receiver::create_manager_configuration()
{
    dpi::ManagerReceiverConfiguration cfg;

    cfg.common.type = dpi::ImageType::TIFF;
    SET_CONFIG_CHILD(common, img_dir);
    cfg.common.info_callback = [](const std::string& s) { ROS_DEBUG_STREAM(s); };
    cfg.common.warning_callback = [](const std::string& s) { ROS_WARN_STREAM(s); };
    cfg.common.error_callback = [](const std::string& s) {  ROS_ERROR_STREAM(s); };

    SET_CONFIG_CHILD(decompressor_defaults, filename_prefix);
    SET_CONFIG_CHILD(decompressor_defaults, rename_partial);

    return cfg;
}

dpi::ros::ReceiverConfiguration dpi::ros::Receiver::create_receiver_configuration()
{
    dpi::ros::ReceiverConfiguration cfg;

    SET_CONFIG_CHILD(received_status, number_of_images_reported);
    
    return cfg;
}


#undef SET_CONFIG
#undef SET_CONFIG_CHILD
