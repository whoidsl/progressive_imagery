/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "progressive_imagery/ros/base.h"

#include "progressive_imagery/TransferProgress.h"


namespace dsl
{
    namespace progressive_imagery
    {
        namespace ros
        {
            struct SenderConfiguration
            {
                struct TransferProgressConfiguration
                {
                    // how many actions to include in the TransferProgress msg
                    int number_of_next_actions { 1000 };
                };

                TransferProgressConfiguration transfer_progress;
            };

            class Sender : public NodeBase
            {
            public:
                Sender();
                void loop() override;

            private:
                void data_request_event() override;
                ManagerBase& manager() override
                { return sender_manager_; }

                ManagerSenderConfiguration create_manager_configuration();
                ros::SenderConfiguration create_sender_configuration();
                void publish_transfer_progress();

            private:
                ManagerSender sender_manager_;
                ros::SenderConfiguration sender_cfg_;
                ::ros::Publisher progress_pub_;
            };
        }
    }
}

namespace dpi = dsl::progressive_imagery;


int main(int argc, char** argv)
{
    ::ros::init(argc, argv, "progressive_imagery_sender");

    dpi::ros::Sender sender;
    sender.run(argc, argv);
}

dpi::ros::Sender::Sender() :
    sender_manager_(create_manager_configuration()),
    sender_cfg_(create_sender_configuration()),
    progress_pub_(node_handle().advertise<::progressive_imagery::TransferProgress>("transfer_progress", 10))
{
    // publish the initial transfer progress
    publish_transfer_progress();
}

void dpi::ros::Sender::loop()
{
    try
    {
        if(sender_manager_.check_for_new_images() > 0)
            publish_transfer_progress(); // new images added, update the transfer progress
    }
    catch(std::exception& e)
    {
        ROS_ERROR_STREAM("Exception while checking for new images: " << e.what());
    }
}


void dpi::ros::Sender::publish_transfer_progress()
{
    auto queue_it = sender_manager_.level2_actions().begin();
    ::progressive_imagery::TransferProgress progress;
    for(int i = 0, n = std::min<int>(sender_cfg_.transfer_progress.number_of_next_actions, sender_manager_.level2_actions().size()); i < n; ++i)
    {
        progress.next_actions.emplace_back();
        auto& tx_action = progress.next_actions.back();
        tx_action.image_id = queue_it->image_it_->compressor().image_id();
        tx_action.fraction_goal = queue_it->action_.fraction_goal;
        ++queue_it;
    }

    progress.entirely_unsent_images = 0;
    for(const auto& image_meta : sender_manager_.level2_image_queue())
    {
        if(image_meta.no_fragments_sent())
            ++progress.entirely_unsent_images;
    }

    progress.sent_but_unacked_fragments = sender_manager_.level1_queue().size();

    progress_pub_.publish(progress);
}


void dpi::ros::Sender::data_request_event()
{
    // we have sent new data
    publish_transfer_progress();
}


// use macros to directly map ROS parameters to library structure values
#define SET_CONFIG(_NAME)                                               \
    node_handle().getParam(#_NAME, cfg._NAME);                           \
    ROS_INFO_STREAM("Parameter " #_NAME " set to " << cfg._NAME);

// map child.name C++ struct access to "child/name" ros parameter
#define SET_CONFIG_CHILD(_CHILD, _NAME)                                 \
    node_handle().getParam(#_CHILD "/" #_NAME, cfg._CHILD._NAME);        \
    ROS_INFO_STREAM("Parameter " #_CHILD "." #_NAME " set to " << cfg._CHILD._NAME);


dpi::ManagerSenderConfiguration dpi::ros::Sender::create_manager_configuration()
{
    dpi::ManagerSenderConfiguration cfg;

    cfg.common.type = dpi::ImageType::TIFF;
    SET_CONFIG_CHILD(common, img_dir);
    cfg.common.info_callback = [](const std::string& s) { ROS_DEBUG_STREAM(s); };
    cfg.common.warning_callback = [](const std::string& s) { ROS_WARN_STREAM(s); };
    cfg.common.error_callback = [](const std::string& s) {  ROS_ERROR_STREAM(s); };

    SET_CONFIG_CHILD(preprocessor_defaults, max_width);
    SET_CONFIG_CHILD(preprocessor_defaults, max_height);
    SET_CONFIG_CHILD(preprocessor_defaults, make_grayscale);


    int progression_order = 0;
    node_handle().getParam("compressor_defaults/progression_order", progression_order);
    switch(progression_order)
    {
        case 0:
            cfg.compressor_defaults.progression_order = dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT;
            ROS_INFO_STREAM("Parameter compressor_defaults.progression_order set to LAYER_RESOLUTION_COMPONENT_PRECINCT");
            break;
        case 1:
            cfg.compressor_defaults.progression_order = dpi::ProgressiveCompressorConfiguration::ProgressionOrder::RESOLUTION_LAYER_COMPONENT_PRECINCT;
            ROS_INFO_STREAM("Parameter compressor_defaults.progression_order set to RESOLUTION_LAYER_COMPONENT_PRECINCT");
            break;
        default:
            ROS_FATAL_STREAM("Invalid progression order value: " << progression_order);
            break;
    }

    SET_CONFIG_CHILD(compressor_defaults, number_of_resolutions);
    SET_CONFIG_CHILD(compressor_defaults, number_of_layers);
    SET_CONFIG_CHILD(compressor_defaults, max_approx_bytes);
    SET_CONFIG_CHILD(compressor_defaults, min_approx_bytes);


    SET_CONFIG(image_filename_regex);
    SET_CONFIG(send_existing_images);
    SET_CONFIG(preprocess_subfolder);
    SET_CONFIG(pass1_fraction);

    return cfg;
}

dpi::ros::SenderConfiguration dpi::ros::Sender::create_sender_configuration()
{
    dpi::ros::SenderConfiguration cfg;

    SET_CONFIG_CHILD(transfer_progress, number_of_next_actions);

    return cfg;
}


#undef SET_CONFIG
#undef SET_CONFIG_CHILD
