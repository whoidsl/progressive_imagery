# Building progressive_imagery

## Dependencies

### Library

The external dependencies are

 * Google Protocol Buffers (>= 2.6.1)
 * DCCL (>= 3.0.6)
 * OpenJPEG (>= 2.3.0)
 * Boost (>= 1.58)
 * Magick++ (>= 6.8.9)

Additionally, for building documentation

 * Doxygen
 * Texlive

In Ubuntu 16.04 these can be installed using

```
# PPA is for DCCL and OpenJPEG 2.3.0
sudo add-apt-repository ppa:tes/dsl
sudo apt update
sudo apt install libprotobuf-dev protobuf-compiler \
     libdccl3-dev dccl3-compiler \
     libopenjp2-7-dev \
     libboost-dev libboost-filesystem-dev \
     libmagick++-dev
# documentation dependencies
sudo apt install doxygen texlive-latex-extra
```

In Ubuntu 20.04, Toby changed how he handled packaging:
```
echo "deb http://packages.gobysoft.org/ubuntu/release/  `lsb_release -c -s`/" \
    | sudo tee /etc/apt/sources.list.d/gobysoft_release.list
sudo apt-key adv --recv-key --keyserver keyserver.ubuntu.com 19478082E2F8D3FE
sudo apt update
```

### ROS

For using the ROS nodes, you will need `ros-kinetic-ros-base.` Newer or older versions of ROS have not been tested. See http://wiki.ros.org/kinetic/Installation.

## Compilation

### Standalone

The library can be compiled standalone using

```
git clone git@git.gobysoft.org:whoi-ndsf/progressive-imagery.git
cd progressive-imagery
mkdir build
cd build
cmake ..
make
```

To enable building of documentation, use

```
cmake .. -Dbuild_doc=ON
make
```

### ROS (Catkin)

Use the standard catkin workspace and commands to build the library and ROS srvs, msgs, and nodes:

Start by creating a workspace if you don't have one already: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

```
cd catkin_ws/src
git clone git@git.gobysoft.org:whoi-ndsf/progressive-imagery.git
cd ..
catkin_make
```
