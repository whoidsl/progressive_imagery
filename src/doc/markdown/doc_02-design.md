# Design

## Project structure

The project is comprised of a library and several ROS nodes. Eventually, support for other middlewares will be completed, as needed.


![](../figures/image-send-structure.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/image-send-structure.png}
\endlatexonly

## Sequence of operations

This figure gives the sequence of the major activities of progressive_imagery when used with ROS (ROS pub/sub/services in **bold**). Green represents outside libraries, yellow are the components included with this project, and blue are hypothetical components that work with this library.

Items marked _async_ can be sent at any time, as desired.

![](../figures/image-send-sequence.png)

\latexonly
\includegraphics[width=1\textwidth]{../figures/image-send-sequence.png}
\endlatexonly

## Sender Queues

The sender uses a three level queuing system. After the JPEG2000 codestream (J2K) is generated, it is comprised of a header and a number of packets (one packet for each quality layer, resolution, color component, and precinct). These packets can be variable sized, so they are further decomposed into a fragments (one or more fragments per packet) of a fixed maximum size (current 249B). These headers and fragments are then queued.

 * Level 2: Actual image headers and fragments. The order of sending from this queue is determined by the action ordering (user priorities, followed by a pass one where a certain fraction of each image is sent in the order they were added to the folder, followed by a pass two where all images are sent in the order they were added to the folder).
 * Level 1: Sent headers and fragments that have not yet been acknowledged.
 * Level 0: Temporary buffer used to store packets that have been requested. This acts as a mask on the Level 1 queue so that multiple calls to request data to not send the same packet repeatedly. This is cleared after each expected or actual transmission from the Receiver to the Sender (acks), so that unacknowledged Level 1 packets are retransmitted.

![](../figures/progressive_imagery_queues.png)

\latexonly
\includegraphics[width=0.9\textwidth]{../figures/progressive_imagery_queues.png}
\endlatexonly
