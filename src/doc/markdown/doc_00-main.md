# Progressive Imagery Main Page

This project provides the following conceptual components:

 * a C++14 library that provides
    * preprocessing of an image to reduce size and color components (grayscale). (See the  dsl::progressive_imagery::PreProcessor)
    * compression of the preprocessed image into JPEG2000 and subsequent packetization into DCCL message header and JPEG2000 packet fragments. (See the dsl::progressive_imagery::ProgressiveCompressor)
    * decompression of the (partial or complete) received image using the available fragments. (See the dsl::progressive_imagery::ProgressiveDecompressor)
    * management of these functions for a set of images in a folder than be added to over time. (See the dsl::progressive_imagery::ManagerSender and dsl::progressive_imagery::ManagerReceiver)
 * A suite of unit tests for all the library components (using gtest)
 * Two ROS nodes that calls the library, one for the Sender (the side where images are being generated), and the Receiver (the source for images).

## Design

See the [Design](doc_02-design.md) page.

## Dependencies/Compilation

See the [Build](doc_01-build.md) page.

## Usage

 * Library: See the Doxygen output. Most users will want to start with the high level APIs given by dsl::progressive_imagery::ManagerSender and dsl::progressive_imagery::ManagerReceiver.
 * [ROS Nodes](doc_10-ros.md)

## Testing

 * Unit tests: A comprehensive suite of units tests is given in `test/unit-tests.cpp` you can run these with

        catkin_make tests && catkin_make run_tests

    Warning: the unit tests will write about 2GB of data to `catkin_ws/build/progressive-imagery/test/data`.
 * [ROS Testing](doc_11-ros_testing.md)