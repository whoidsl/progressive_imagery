# ROS Nodes

All parameters, services, and topic names are private (i.e. using NodeHandle('~')).

## Sender node

This node is to be run on the platform that is generating images (e.g. AUV). This provides a ROS interface to the dsl::progressive_imagery::ManagerSender.

### Parameters

The YAML parameters are a one-to-one mapping of the dsl::progressive_imagery::ManagerSenderConfiguration struct. Omitted parameters will use the defaults given in the definition of dsl::progressive_imagery::ManagerSenderConfiguration.

For example:

```
common:
  img_dir: "/home/gobysoft/Desktop/test-images"
image_filename_regex: "^image_([0-9]*)\\.[a-zA-Z]{3,4}$"
preprocessor_defaults:
  max_width: 1920
  max_height: 1080
  make_grayscale: true
compressor_defaults:
  progression_order: 0 # LRCP = 0, RLCP = 1
  number_of_resolutions: 6
  number_of_layers: 10
  max_approx_bytes: 20480
  min_approx_bytes: 640
send_existing_images: true
preprocess_subfolder: "preprocess"
pass1_fraction: 0.2
```

In addition, you can specify the maximum number of actions to include in the `~/transfer_progress` publication.

```
transfer_progress:
  number_of_next_actions: 20
```


### Services

Since the image data varies with the current the user requested queues, the data must be requested immediately prior to transmission. This is implemented as a ROS service, where the data link layer interface ("modemdriver") is the client, and this Sender node is the server.

| Name | Type | Request | Response|
|------|------|---------|-------- |
| outgoing_data | ModemDataRequest.srv | Max `payload` of `bytes_requested`. Set `reset_unacknowledged==true` if this is the first request since acks were or would have been returned. This clears the staging queue and thus starts resending previously sent unacknowledged messages. Set `reset_unacknowledged==false` for subsequent requests before acks.| AcousticModemData packet containing compressed DCCL header and/or fragment packets. If no data, the payload vector is empty. |

For example, filling a rate 5 Micro-Modem packet, which is 8 frames of 256 bytes:

```
for(int i = 0; i < 8; ++i) {
   req.bytes_requested = 256;
   req.reset_unacknowledged = (i == 0) ? true : false;
   client.call(req);
}
```

### Publishes

| Name | Type | Description | Frequency |
|------|------|-------------|-----------|
| transfer_progress | TransferProgress.msg | The number of `entirely_unsent_messages`, the list of the `next_actions` to take (e.g. send image id N to 50% [fraction = 0.5]), and the number of `unsent_but_unacked_fragments` (i.e. the size of the level 1 queue). `next_actions` is limited to the number specified in the parameters at launch. | When a new image is added to the folder, or when the `outgoing_data` service is called |


### Subscriptions

| Name | Type | Description |
|------|------|-------------|
| incoming_data | AcousticModemData.msg | Data received from the Receiver (should be an identical payload to the response to the Receiver's `outgoing_data` service). Out-of-order and/or duplicate packets are acceptable. |
| queue_update | QueueAction.msg | Vector of desired queue goals that will take precedence to the default transmission goals. Subsequent publications to this topic will overwrite any previous queue updates.|


## Receiver node

This node is to be run on the platform receiving images (e.g. User Interface). This provides a ROS interface to the dsl::progressive_imagery::ManagerReceiver.

### Parameters

The YAML parameters are a one-to-one mapping of the dsl::progressive_imagery::ManagerReceiverConfiguration struct. Omitted parameters will use the defaults given in the definition of dsl::progressive_imagery::ManagerReceiverConfiguration.


For example:

```
common:
  img_dir: "/home/gobysoft/Desktop/test-images/rx"
decompressor_defaults:
  filename_prefix: "image_rx_"
  rename_partial: true
```

In addition, you can specify the length of the vector reported in the `~/received_status` topic:

```
received_status:
  number_of_images_reported: 20
```


### Services


| Name | Type | Request | Response| Notes |
|------|------|---------|-------- |-------|
| outgoing_data | ModemDataRequest.srv | Max `payload` of `bytes_requested`. The value of  `reset_unacknowledged` is not used. | AcousticModemData packet containing compressed DCCL combined ACK / queue update packets. If no data, the payload vector is empty. | Symmetric call to the Sender service of the same name |

### Publishes

| Name | Type | Description | Frequency |
|------|------|-------------|-----------|
| received_status | ReceivedStatus.msg | A vector of image ids and the fraction [0,1] of the compressed image received thus far | Upon receipt of `incoming_data` | 
| updated_image_event | UpdatedImageEvent.msg | A vector of image ids and file paths of images that were just created or re-written with new data | Upon receipt of `incoming_data` |

### Subscriptions

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| incoming_data | AcousticModemData.msg | Data received from the Sender. Out-of-order and/or duplicate packets are acceptable. | Symmetric to the Sender subscription of the same name |
| queue_update | QueueAction.msg | Vector of desired queue goals that will take precedence to the default transmission goals. Subsequent publications to this topic will overwrite any previous queue updates.| Symmetric to the Sender subscription of the same name |