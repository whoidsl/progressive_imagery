# ROS Testing

Much of the core library functionality is tested using the unit test suite (`test/unit-tests.cpp`).

All the ROS testing tools are in `test/ros` and the configuration (XML, YAML) used in `test/launch`. These tests were performed to validate functionality of the ROS service and pub/sub interfaces, and to ensure the queue updates function properly. In addition, they tested for reasonable CPU and memory loads. Since the unit tests exist, they were not as comprehensive in testing all edge cases.

## Sample images

The sample images used were 9.0MB to 25MB in size, and were provided from historical Sentry data:

```
9MB  sentry.20160731.042814259898.1061.acun.tif
25MB sentry.20160731.043104258818.1112.acun.tif
25MB sentry.20160731.045617582510.1566.acun.tif
17MB sentry.20160731.053557497310.2280.acun.tif
```

## UDP tests

The `ModemDriverSim` ROS node was written to implement the link layer interface to the Sender and Receiver ROS nodes and forward the data using point-to-point UDP/IP datagrams.

This was paired with the DRCTrafficControl simulator (https://github.com/GobySoft/drc_tc from the 2013-2015 DARPA Robotics Challenge) which allows arbitrary network degradation (packet loss, latency, etc.) on a IP tunnel interface.

Various settings were tested, but most tests were performed with a 1 second TDMA and the following DRCTrafficControl settings:

```
latency_ms: 200
drop_percentage: 50
do_rate_control: true
max_rate: 700
rate_unit: BYTES_PER_SEC
```

This was designed to roughly simulate a poor acomms channel (random 50% packet loss) but at about 15x the real speed to allow for faster test iterations.

Testing was performed on a virtual machine running on a Intel Core i7-5820K CPU @ 3.30GHz (6 cores) running Ubuntu 16.04 amd64 and compiled with GCC 5.4.0 with C++14 standards enabled. In addition, they were performed on a Raspberry Pi Compute Module 3 (BCM2837 CPU  @ 1.2GHz) using Ubuntu 16.04 armhf compiled with GCC 5.4.0 with C++14 enabled.

## Launching the tests

```
cd test/launch
roslaunch test.launch
```

In addition, it was useful to monitor sender and receiver statuses:

```
rostopic echo /sender/transfer_progress
rostopic echo /receiver/received_status
```

In a real system, it may be useful to transfer `/sender/transfer_progress` over the comms link as well to the receiver side.

## Tests performed

All tests ensured that 100% of all images were eventually transferred, and that the images were transferred in the order desired by the user specified queue updates and by the defaults. The test images were provided by the WHOI Deep Submergence Lab and are in `test/data`. Multiple copies of the same image were used as necessary.

Queue updates were tested by manually publishing to `/receiver/queue_update` and `/sender/queue_update`.

For example:

```
rostopic pub -1 /receiver/queue_update progressive_imagery/QueueAction \
   '{ queue_action: [{ image_id: 2, fraction_goal: 0.5 }]}'
```

The test scenarios were:

 * No images in the folder at launch. Two images added.
 * Several images in the folder at launch. All images sent to 100% then another image was added. The new image was transmitted to 100%.
 * Manual queue updates requested for images that had not yet been sent to that goal. Ensure that the transmission switches to the desired updated goal(s).
 * Manual queue updates requested for images that had already been sent to that goal (these are immediately superceded).

## Raspberry Pi Testing

This testing was performed to ensure the load and functionality was satisfactory on an embedded ARM computer. The sender was run on the Raspberry Pi, and the receiver was run on the amd64 VM, as per the above tests.

Installed the Raspberry Pi using the Ubuntu 16.04 image from WHOI CGSN:
http://gobysoft.org/doc/cgsn_mooring/cgsn-mooring-doc/html/md__root_cgsn-mooring_src_doc_markdown_doc_02-embedded-image.html

On the VM:

```
roslaunch test-receiver-rp.launch 
```

On the Raspberry Pi:

```
roslaunch test-sender-rp.launch
```

The same set of tests were performed using the Raspberry Pi as the sender. Memory usage was around 1.8% (17.2MB). CPU usage was at 2-3% except when encoding a new image, when it would go to 100% for about 5 seconds.
