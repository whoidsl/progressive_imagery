/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include <boost/filesystem.hpp>

#include "progressive_imagery/exception.h"
#include "progressive_imagery/manager.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace dpi = dsl::progressive_imagery;


dsl::progressive_imagery::ManagerBase::ManagerBase(ManagerCommonConfiguration cfg) :
    cfg_(std::move(cfg))
{
    dccl_.load<dsl::protobuf::J2KMainHeader>();
    dccl_.load<dsl::protobuf::J2KPacketFragment>();
    dccl_.load<dsl::protobuf::J2KQueueUpdateAndAck>();

    try
    {
        if(!boost::filesystem::exists(cfg_.img_dir) || !boost::filesystem::is_directory(cfg_.img_dir))
            throw(dsl::progressive_imagery::Exception(std::string(cfg_.img_dir + " directory does not exist or is not a directory.")));

        // write a test file to check that we can write to this directory
        {
            std::string test_file(std::string(cfg_.img_dir + "/" + ".progressive_codec_manager_test"));
            std::ofstream fout(test_file.c_str());
            if(!fout.is_open())
                throw(Exception("Failed to write to directory: " + cfg_.img_dir));
            boost::filesystem::remove(test_file);
        }
    }
    catch (const boost::filesystem::filesystem_error& e)
    {
        throw(dsl::progressive_imagery::Exception(e));
    }
}


//
// ManagerSender
//
dpi::ManagerSender::ManagerSender(ManagerSenderConfiguration cfg)
    : ManagerBase(cfg.common),
      cfg_(std::move(cfg)),
      filename_regex_(cfg_.image_filename_regex)
{
    set_codec_defaults(cfg_.compressor_defaults);

    // regex must capture exactly one value
    if(filename_regex_.mark_count() != 1)
        throw(Exception("image_filename_regex must capture exactly one subexpression using (), which represents the image id"));

    if(cfg_.send_existing_images)
    {
        std::vector<boost::filesystem::path> files;
        for(const auto& directory_entry : boost::filesystem::directory_iterator(cfg_.common.img_dir))
        {
            if(directory_entry.status().type() == boost::filesystem::regular_file)
                files.push_back(directory_entry.path());
        }

        std::sort(files.begin(), files.end());
        for(const auto& path : files)
            queue_image(path);
    }

    // add watch events for inotify
    if((inotify_wd_ = inotify_add_watch(inotify_fd_, cfg_.common.img_dir.c_str(), IN_CLOSE_WRITE) == -1))
        throw(Exception("Failed to add inotify watch on " + cfg_.common.img_dir));



}

int dpi::ManagerSender::check_for_new_images()
{
    int num_images = 0;

    const struct inotify_event *event;
    int len = read(inotify_fd_, inotify_buffer_.data(), inotify_buffer_.size());

    if(len == -1 && errno != EAGAIN)
        throw(Exception("Failed to call read on inotify fd"));

    if(len <= 0) return num_images;


    for (char* ptr = inotify_buffer_.data();
         ptr < inotify_buffer_.data() + len;
         ptr += sizeof(struct inotify_event) + event->len)
    {
        event = reinterpret_cast<const struct inotify_event *>(ptr);

        if (event->mask & IN_CLOSE_WRITE)
        {
            if (event->len)
            {
                if(queue_image(cfg_.common.img_dir + "/" + event->name))
                    ++num_images;
            }
        }
    }

    return num_images;
}

bool dpi::ManagerSender::queue_image(const boost::filesystem::path& image_path)
{

    std::smatch match;
    std::string filename = image_path.filename().native();
    std::regex_search(filename, match, filename_regex_);


    if(match.empty())
    {
        PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                             "File: " << image_path << " does not match image_filename regex, ignoring.");
    }
    else
    {
        boost::filesystem::path image_preprocess_path = image_path.parent_path();

        if(!cfg_.preprocess_subfolder.empty())
            image_preprocess_path /= cfg_.preprocess_subfolder;

        if(!boost::filesystem::exists(image_preprocess_path))
            boost::filesystem::create_directory(image_preprocess_path);

        auto image_out_dir = image_preprocess_path;

        image_preprocess_path /= image_path.stem();
        image_preprocess_path += ".preprocessed";
        image_preprocess_path += image_path.extension();

        PreProcessorConfiguration pp_cfg = cfg_.preprocessor_defaults;
        pp_cfg.in_file = image_path.native();
        pp_cfg.out_file = image_preprocess_path.native();

        {
            dpi::PreProcessor pp(pp_cfg);
            pp.process();
            pp.write();
        }


        try
        {

            ProgressiveCompressorConfiguration compressor_config = cfg_.compressor_defaults;
            compressor_config.common.out_dir = image_out_dir.native();
            compressor_config.common.type = cfg_.common.type;

            compressor_config.image_id = std::stoi(match[1]);
            compressor_config.in_file = image_preprocess_path.native();

            if(!image_id_to_level2_queue_it_.count(compressor_config.image_id))
            {
                ImageTxMeta new_meta(ProgressiveCompressor({ compressor_config }));
                level2_image_queue_.emplace_back(std::move(new_meta));
                auto new_image_it = level2_image_queue_.end(); --new_image_it;

                new_image_it->compressor().compress();

                if(cfg_.encode_callback)
                {
                    std::shared_ptr<dsl::protobuf::J2KMainHeader> header;
                    std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>> fragments;
                    std::tie(header, fragments) = new_image_it->compressor().packetize();

                    int dccl_encoded_bytes = dccl().size(*header);
                    for(auto fragment : fragments)
                        dccl_encoded_bytes += dccl().size(*fragment);

                    cfg_.encode_callback(*header, dccl_encoded_bytes);
                }

                // pass 2, send everything
                level2_actions_.push_back({ new_image_it, { compressor_config.image_id, static_cast<float>(1.0) }});

                // pass 1, inserted before the beginning of the pass 2 actions
                // first find the beginning of the level2 actions
                auto level2_begin_it = level2_actions_.begin();
                while(level2_begin_it != level2_actions_.end() && level2_begin_it->action_.fraction_goal < 1.0)
                    ++level2_begin_it;

                // insert the action with the default pass 1 fraction set
                level2_actions_.insert(level2_begin_it, { new_image_it, { compressor_config.image_id, cfg_.pass1_fraction }});

                // keep track of image id -> vector iterator since we are
                // not requiring that the image ids are in order
                image_id_to_level2_queue_it_.emplace(compressor_config.image_id, new_image_it);

                PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                                     "Queued: image id: " << compressor_config.image_id << ": " << image_path);

                return true;
            }
            else
            {
                throw(Exception("Image ID " + std::to_string(compressor_config.image_id) + " already queued."));
            }
        }
        catch(std::exception& e)
        {
            throw(Exception(std::string("Failed to queue image: ") + image_path.native() + ": " + e.what()));
        }
    }

    return false;
}



//
// send any unacked messages (level 1) that haven't been sent since the staging queue (level 0) was cleared
// if requests exist, process these
// otherwise, send default % of all images
// if all images have default % sent and acked, send the remainder of images starting from
// if all images have all % sent and acked, return an empty vector
std::vector<std::shared_ptr<google::protobuf::Message>> dpi::ManagerSender::provide_compressed_data(std::vector<unsigned char>& data, int max_bytes)
{
    data.clear();

    std::vector<std::shared_ptr<google::protobuf::Message>> msgs;

    //
    // pull from level 1
    //

    if(level1_queue_.size() > level0_queue_.size())
    {
        for(const auto& level1_pair : level1_queue_)
        {
            // send this if it hasn't been sent since the last time the staging (level 0) queue was cleared
            if(!level0_queue_.count(level1_pair.first))
            {
                if(!encode(data, max_bytes, level1_pair.second))
                    return msgs;
                msgs.push_back(level1_pair.second);
                level0_queue_.insert(level1_pair);
            }
        }
    }

    //
    // pull from level 2
    //

    // nothing to do
    if(level2_actions_.empty())
       return msgs;

    auto* meta = &(*level2_actions_.begin()->image_it_);
    auto send_fraction_goal = level2_actions_.begin()->action_.fraction_goal;
    if(!meta->image_data_is_allocated())
        meta->allocate_image_data();

    auto check_goal_met = [this](ImageTxMeta*& meta, float& send_fraction_goal)->bool
        {  // have we met the send goal?
            while(meta->all_fragments_sent() ||
                  meta->sent_fraction() >= send_fraction_goal)
            {

                meta->deallocate_image_data();

                level2_actions_.pop_front();
                if(level2_actions_.empty())
                    return false;


                meta = &(*level2_actions_.begin()->image_it_);
                send_fraction_goal = level2_actions_.begin()->action_.fraction_goal;

                PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                                     "Moving to next goal: id: " << meta->compressor().image_id() << ", fraction: " << send_fraction_goal);

                meta->allocate_image_data();
            }
            return true;
        };

    if(!check_goal_met(meta, send_fraction_goal))
        return msgs;

    while(data.size() < max_bytes)
    {
        auto next_message = meta->peek_next_message();
        if(next_message)
        {
            if(!encode(data, max_bytes, next_message)) // data would be too large if we added this message
            {
                return msgs;
            }
            else
            {
                auto next_message_pair = meta->get_next_message();
                level0_queue_.emplace(next_message_pair);
                level1_queue_.emplace(next_message_pair);
                msgs.push_back(next_message_pair.second);


                PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                                     "id: " << meta->compressor().image_id() << ": % sent: " << 100*meta->sent_fraction());

                if(!check_goal_met(meta, send_fraction_goal))
                    return msgs;
            }
        }
        else
        {
            throw(Exception("Next message is null, even though fractional send goal hasn't been met."));

        }
    }
    return msgs;
}


std::set<int> dpi::ManagerSender::post_compressed_data(const std::vector<unsigned char>& ack_data)
{
    std::set<int> all_updated_images;

    auto ack_dccl_id = dccl().id<dsl::protobuf::J2KQueueUpdateAndAck>();
    auto next_it = ack_data.begin();
    while(next_it != ack_data.end())
    {
        auto dccl_id = dccl().id(next_it, ack_data.end());
        if(dccl_id == ack_dccl_id)
        {
            dsl::protobuf::J2KQueueUpdateAndAck queue_update_and_ack;
            next_it = dccl().decode(next_it, ack_data.end(), &queue_update_and_ack);
            auto updated_images = post_queue_update_and_ack(queue_update_and_ack);
            all_updated_images.insert(updated_images.begin(), updated_images.end());
        }
        else
        {
            PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                                 "Unexpected DCCL id: " << dccl_id << ", discarding message.");
            break;
        }
    }

    return all_updated_images;
}

std::set<int> dpi::ManagerSender::post_queue_update_and_ack(const dsl::protobuf::J2KQueueUpdateAndAck& queue_update_and_ack)
{
    std::set<int> updated_images;
    for(const auto& ack : queue_update_and_ack.ack())
    {
        auto level2_queue_it = image_id_to_level2_queue_it_.find(ack.image_id());
        if(level2_queue_it != image_id_to_level2_queue_it_.end())
        {
            level2_queue_it->second->post_ack_message(ack);
            level1_queue_.erase(ack);
            level0_queue_.erase(ack);
            updated_images.insert(ack.image_id());
        }
        else
        {
            PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                                 "Received ack for image id we do not have loaded.");
        }
    }

    // update user requests
    if(queue_update_and_ack.queue_action_size() > 0)
    {
        std::vector<dpi::QueueAction> actions;
        for(auto dccl_action : queue_update_and_ack.queue_action())
            actions.push_back( { static_cast<int>(dccl_action.image_id()), dccl_action.fraction_goal() } );

        post_queue_update(actions);
    }
    return updated_images;
}




void dpi::ManagerSender::post_queue_update(const std::vector<QueueAction>& actions)
{
    // remove the previous user actions
    auto it = level2_actions_.begin(), end = level2_actions_.end();
    while(it != end && it->user_action_ == true)
    {
        it->image_it_->deallocate_image_data();
        it = level2_actions_.erase(it);
    }

    // add the new actions
    for(auto it = actions.rbegin(), end = actions.rend(); it != end; ++it)
    {
        if(it->fraction_goal >= 0 && it->fraction_goal <= 1)
        {
            auto id2queue_it = image_id_to_level2_queue_it_.find(it->image_id);
            if(id2queue_it != image_id_to_level2_queue_it_.end())
            {
                level2_actions_.push_front(ImageTxAction({ id2queue_it->second, *it, true }));
            }
            else
            {
                PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                                     "Ignoring queue request for image ID: " << it->image_id << " that doesn't exist");
            }
        }
        else
        {
            PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                                 "Ignoring queue request for fraction: " << it->fraction_goal << " that isn't in [0, 1]");
        }
    }
}



// ImageTxMeta

void dpi::ManagerSender::ImageTxMeta::allocate_image_data()
{
    using std::rel_ops::operator<=;

    std::tie(header_, fragments_) = compressor_.packetize();
    number_fragments_plus_header_ = fragments_.size() + 1;

    next_fragment_ = fragments_.begin();
    if(!fragments_sent_.empty())
    {
        // search for the last fragment sent and set the iterator appropriately
        const auto& last_fragment_sent = *fragments_sent_.rbegin();
        while(next_fragment_ != fragments_.end() && (*next_fragment_)->id() <= last_fragment_sent)
            ++next_fragment_;

    }
}

void dpi::ManagerSender::ImageTxMeta::deallocate_image_data()
{
    header_.reset();
    fragments_.clear();
}

std::shared_ptr<google::protobuf::Message> dpi::ManagerSender::ImageTxMeta::peek_next_message()
{
    if(!header_sent())
        return header_;
    else if(next_fragment_ != fragments_.end())
        return *next_fragment_;
    else
        return std::shared_ptr<google::protobuf::Message>();
}

std::pair<dsl::protobuf::J2KPacketFragmentIdentifier, std::shared_ptr<google::protobuf::Message>>
                              dpi::ManagerSender::ImageTxMeta::get_next_message()
{
    if(!header_sent())
    {
        dsl::protobuf::J2KPacketFragmentIdentifier header_id;
        header_id.set_image_id(header_->image_id());
        header_id.set_packet_id(HEADER_PACKET_ID);
        header_id.set_fragment_id(0);
        header_id.set_is_last_fragment(true);

        fragments_sent_.insert(header_id);
        return std::make_pair(header_id, header_);
    }
    else if(next_fragment_ != fragments_.end())
    {
        auto& fragment_id = (*next_fragment_)->id();
        fragments_sent_.insert(fragment_id);
        return std::make_pair(fragment_id, *(next_fragment_++));
    }
    else
    {
        return std::make_pair(dsl::protobuf::J2KPacketFragmentIdentifier(),
                              std::shared_ptr<google::protobuf::Message>());
    }
}


//
// ManagerReceiver
//
std::set<int> dpi::ManagerReceiver::post_compressed_data(const std::vector<unsigned char>& data)
{
    std::set<int> all_updated_images;

    const auto header_dccl_id = dccl().id<dsl::protobuf::J2KMainHeader>();
    const auto fragment_dccl_id = dccl().id<dsl::protobuf::J2KPacketFragment>();

    auto next_it = data.begin();
    while(next_it != data.end())
    {
        auto dccl_id = dccl().id(next_it, data.end());
        if(dccl_id == header_dccl_id)
        {
            dsl::protobuf::J2KMainHeader header_out;
            next_it = dccl().decode(next_it, data.end(), &header_out);
            post_header(header_out);
        }
        else if(dccl_id == fragment_dccl_id)
        {
            dsl::protobuf::J2KPacketFragment fragment_out;
            next_it = dccl().decode(next_it, data.end(), &fragment_out);
            auto updated_images = post_fragment(fragment_out);
            all_updated_images.insert(updated_images.begin(), updated_images.end());
        }
        else
        {
            PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                                 "Unexpected DCCL id: " << dccl_id << ", discarding message.");
            break;
        }
    }

    return all_updated_images;
}

void dpi::ManagerReceiver::post_header(const dsl::protobuf::J2KMainHeader& header_out)
{
    ProgressiveDecompressorConfiguration decompressor_config = cfg_.decompressor_defaults;
    decompressor_config.common.type = cfg_.common.type;

    rx_images_.emplace(header_out.image_id(),
                       ImageRxMeta( { dpi::ProgressiveDecompressor(decompressor_config, header_out) }));

    dsl::protobuf::J2KPacketFragmentIdentifier ack_id;
    ack_id.set_image_id(header_out.image_id());
    ack_id.set_packet_id(HEADER_PACKET_ID);
    ack_id.set_fragment_id(0);
    ack_id.set_is_last_fragment(true);
    pending_acks_.insert(ack_id);
}

std::set<int> dpi::ManagerReceiver::post_fragment(const dsl::protobuf::J2KPacketFragment fragment_out)
{
    std::set<int> updated_images;
    auto rx_image_it = rx_images_.find(fragment_out.id().image_id());
    if(rx_image_it != rx_images_.end())
    {
        auto& decompressor = rx_image_it->second.decompressor;
        decompressor.add_fragment(fragment_out);

        rx_image_it->second.fraction_available = decompressor.decompress();
        updated_images.insert(decompressor.image_id());

        pending_acks_.insert(fragment_out.id());
    }
    else
    {
        PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                             "Received fragment for image " << fragment_out.id().image_id() << " that we haven't yet received a header for. Discarding fragment");
    }
    return updated_images;
}



std::vector<std::shared_ptr<google::protobuf::Message>> dpi::ManagerReceiver::provide_compressed_data(std::vector<unsigned char>& data, int max_bytes)
{
    data.clear();
    std::vector<std::shared_ptr<google::protobuf::Message>> msgs;

    const auto acks_per_message = dsl::protobuf::J2KQueueUpdateAndAck::descriptor()->FindFieldByName("ack")->options().GetExtension(dccl::field).max_repeat();

    auto pending_ack_it = pending_acks_.begin();
    while(data.size() < max_bytes && pending_ack_it != pending_acks_.end())
    {
        auto pending_ack_it_begin = pending_ack_it;

        auto dccl_ack = std::make_shared<dsl::protobuf::J2KQueueUpdateAndAck>();

        // if we haven't send the pending queue actions before, send them now
        bool add_pending_queue_actions = !latest_ack_with_queue_action_.IsInitialized();

        while(pending_ack_it != pending_acks_.end())
        {
            if(dccl_ack->ack_size() == acks_per_message)
                break;

            *dccl_ack->add_ack() = *pending_ack_it;

            // if we're resending the ack that was paired with the queue actions before, send them again
            if(*pending_ack_it == latest_ack_with_queue_action_)
                add_pending_queue_actions = true;

            ++pending_ack_it;
        }

        if(add_pending_queue_actions)
        {
            for(const auto& action : pending_queue_actions_)
            {
                auto& dccl_action = *dccl_ack->add_queue_action();
                dccl_action.set_image_id(action.image_id);
                dccl_action.set_fraction_goal(action.fraction_goal);
            }
            if(dccl_ack->ack_size() > 0)
                latest_ack_with_queue_action_ = dccl_ack->ack(dccl_ack->ack_size()-1);
        }

        if(!encode(data, max_bytes, dccl_ack))
        {
            return msgs;
        }
        else
        {
            // erase the acks from pending_acks
            pending_acks_.erase(pending_ack_it_begin, pending_ack_it);
            msgs.push_back(dccl_ack);
            PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                                 "Sending ack message: " << dccl_ack->ShortDebugString());

        }
    }
    return msgs;

}

void dpi::ManagerReceiver::post_queue_update(const std::vector<QueueAction>& actions)
{
    auto max_queue_updates = protobuf::J2KQueueUpdateAndAck::descriptor()->FindFieldByName("queue_action")->options().GetExtension(dccl::field).max_repeat();
    if(actions.size() > max_queue_updates)
    {
        PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                             "QueueAction update must contain fewer than " << max_queue_updates << " updates. Truncating");

        pending_queue_actions_.assign(actions.begin(), actions.begin() + max_queue_updates);
    }
    else
    {
        pending_queue_actions_ = actions;
    }

    latest_ack_with_queue_action_.Clear();

}
