/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include <cmath>


#include "progressive_imagery/progressive_codec.h"

extern "C"
{
#include "progressive_imagery/openjpeg_convert/convert.h"
}

#include <iostream>
#include <sstream>
#include <iomanip>

namespace dpi = dsl::progressive_imagery;

static void error_callback(const char *msg, void *client_data)
{    
    auto* callback = (std::function<void(const std::string&)>*)client_data;    
    if(callback && *callback) (*callback)(std::string("[OpenJPEG: ERROR] ") + msg);
}

static void warning_callback(const char *msg, void *client_data)
{    
    auto* callback = (std::function<void(const std::string&)>*)client_data;

    // reject this annoying message.
    if(std::string(msg).find("Not enough space for expected SOP marker") != std::string::npos)
        return;

    if(callback && *callback) (*callback)(std::string("[OpenJPEG: WARNING] ") + msg);
}

static void info_callback(const char *msg, void *client_data)
{    
    auto* callback = (std::function<void(const std::string&)>*)client_data;
    if(callback && *callback) (*callback)(std::string("[OpenJPEG: INFO] ") + msg);
}


//
// COMPRESSOR
//
dpi::ProgressiveCompressor::ProgressiveCompressor(ProgressiveCompressorConfiguration cfg) try :
    cfg_(std::move(cfg)),
    image_in_path_(cfg_.in_file),
    image_(nullptr, opj_image_destroy)
{
    //
    // check configuration
    //

    // decomposition levels = number of resolutions - 1 (see standard)
    const auto& decomp_options = dsl::protobuf::J2KMainHeader::COD::descriptor()->FindFieldByName("spcod_number_of_decomp_levels")->options().GetExtension(dccl::field);
    auto max_resolutions = decomp_options.max() + 1;
    auto min_resolutions = decomp_options.min() + 1;
    if(cfg.number_of_resolutions > max_resolutions)
        throw(Exception(std::string("number_of_resolutions must not be greater than ") + std::to_string(max_resolutions) + std::string(", currently set to: ") + std::to_string(cfg.number_of_resolutions)));
    if(cfg.number_of_resolutions < min_resolutions)
        throw(Exception(std::string("number_of_resolutions must not be less than ") + std::to_string(min_resolutions) + std::string(", currently set to: ") + std::to_string(cfg.number_of_resolutions)));

    // layers
    const auto& layer_options = dsl::protobuf::J2KMainHeader::COD::descriptor()->FindFieldByName("sgcod_number_of_layers")->options().GetExtension(dccl::field);
    auto max_layers = layer_options.max();
    auto min_layers = layer_options.min();
    if(cfg.number_of_layers > max_layers)
        throw(Exception(std::string("number_of_layers must not be greater than ") + std::to_string(max_layers) + std::string(", currently set to: ") + std::to_string(cfg.number_of_layers)));
    if(cfg.number_of_layers < min_layers)
        throw(Exception(std::string("number_of_layers must not be less than ") + std::to_string(min_layers) + std::string(", currently set to: ") + std::to_string(cfg.number_of_layers)));
        

    if(!boost::filesystem::exists(image_in_path_) || boost::filesystem::is_directory(image_in_path_))
        throw(Exception(std::string(cfg_.in_file + " file does not exist or is a directory.")));
    
    if(!boost::filesystem::exists(cfg_.common.out_dir) || !boost::filesystem::is_directory(cfg_.common.out_dir))
        throw(Exception(std::string(cfg_.common.out_dir + " directory does not exist or is not a directory.")));

    j2k_out_path_ = cfg_.common.out_dir;
    j2k_out_path_ /= image_in_path_.stem();
    j2k_out_path_ += ".j2k";

    {
        // open/create the file to check that it is writable now, rather than waiting for compress()
        std::ofstream fout(j2k_out_path_.c_str());
        if(!fout.is_open())
            throw(Exception("Failed to open file for writing: " + j2k_out_path_.native()));
        boost::filesystem::remove(j2k_out_path_);
    }


    // Set encoding parameters to default values (removed those overwritten below):
    // 1 tile
    // Size of precinct : 2^15 x 2^15 (means 1 precinct)
    // Size of code-block : 64 x 64
    // No sub-sampling in x or y direction
    // No mode switch activated
    // No index file
    // No ROI upshifted
    // No offset of the origin of the image
    // No offset of the origin of the tiles
    opj_set_default_encoder_parameters(&parameters_);
    
    switch(cfg_.common.type)
    {
        case ImageType::TIFF:
            image_.reset(tiftoimage(cfg_.in_file.c_str(), &parameters_));
            break;

        case ImageType::PNG:
            image_.reset(pngtoimage(cfg_.in_file.c_str(), &parameters_));
            break;

        case ImageType::PPM:
            image_.reset(pnmtoimage(cfg_.in_file.c_str(), &parameters_));
            break;

    }

    if(!image_)
        throw(Exception("Unable to load in_file using OpenJPEG *toimage()"));
    
    
    //
    // set up compression parameters
    //

    // enable RGB->YCC conversion (tcp_mct == 1) if image has at least three color components
    parameters_.tcp_mct = (image_->numcomps >= 3) ? 1 : 0;

    // number of layers + rates
    parameters_.tcp_numlayers = cfg_.number_of_layers;

    // tcp_rate_t is a floating point numeric 
    using tcp_rate_t = std::remove_all_extents<decltype(parameters_.tcp_rates)>::type;
    tcp_rate_t image_bytes = image_->x1*image_->y1*image_->numcomps*image_->comps[0].bpp/8;
    auto log_layer_min = std::log10(image_bytes/cfg_.min_approx_bytes);
    auto log_layer_max = std::log10(image_bytes/cfg_.max_approx_bytes);
    auto dlog_layer = (log_layer_min - log_layer_max) / (cfg_.number_of_layers - 1);
    
    for(auto i = 0; i < cfg_.number_of_layers; ++i)
    {
        auto rate = std::pow(10.0, log_layer_min - dlog_layer*i);
        if(rate < 1)
        {
            parameters_.tcp_rates[i] = 1;
            PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                                 "stopping at " << i+1 << " layers since tcp_rates < 1");
            parameters_.tcp_numlayers = i+1;
            break;
        }
        else
        {
            parameters_.tcp_rates[i] = rate;
        }
    }
    
    parameters_.cp_disto_alloc = 1;
    
    // number of resolutions
    parameters_.numresolution = cfg_.number_of_resolutions;
    
    // enable Start-Of-Packet (SOP) marker
    parameters_.csty |= 0x02;    
    
    // enable Irreversible color compression
    parameters_.irreversible = 1;
    
    // set progression order
    switch(cfg_.progression_order)
    {
        case ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT:
            parameters_.prog_order = OPJ_LRCP;
            break;
        case ProgressiveCompressorConfiguration::ProgressionOrder::RESOLUTION_LAYER_COMPONENT_PRECINCT:
            parameters_.prog_order = OPJ_RLCP;
            break;
    }

    
}
catch (const boost::filesystem::filesystem_error& e)
{
    throw(dpi::Exception(e));
}

dpi::ProgressiveCompressor::~ProgressiveCompressor()
{
}


void dpi::ProgressiveCompressor::compress()
{    
    std::unique_ptr<opj_codec_t, decltype(&opj_destroy_codec)> codec(
        opj_create_compress(OPJ_CODEC_J2K),
        opj_destroy_codec);

    if(!codec)
        throw(Exception("Failed to create codec: opj_create_compress"));
    
    opj_set_info_handler(codec.get(), info_callback, &cfg_.common.info_callback);
    opj_set_warning_handler(codec.get(), warning_callback, &cfg_.common.warning_callback);
    opj_set_error_handler(codec.get(), error_callback, &cfg_.common.error_callback);
    
    if(!opj_setup_encoder(codec.get(), &parameters_, image_.get()))
        throw(Exception("Failed to encode image: opj_setup_encoder"));

    std::unique_ptr<opj_stream_t, decltype(&opj_stream_destroy)> stream(
        opj_stream_create_default_file_stream(j2k_out_path_.c_str(), OPJ_FALSE),
        opj_stream_destroy);

    if(!stream)
        throw(Exception("Failed to create stream: opj_create_create_default_file_stream"));

    ImageRemover image_remover(j2k_out_path_);
    
    if(!opj_start_compress(codec.get(), image_.get(), stream.get()))
        throw(Exception("Failed to encode image: opj_start_compress"));
    
    if(!opj_encode(codec.get(), stream.get()))
        throw(Exception("Failed to encode image: opj_encode"));

    if(!opj_end_compress(codec.get(), stream.get()))
        throw(Exception("Failed to encode image: opj_end_compress"));

    // we have finished compression successfully, inform image_remover not to delete the image
    image_remover.commit();
}
        
std::tuple<std::shared_ptr<dsl::protobuf::J2KMainHeader>, std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>>> dpi::ProgressiveCompressor::packetize()
{
    std::ifstream j2k_ifs(j2k_out_path_.c_str(), std::ios::in | std::ios::binary);
    if(!j2k_ifs.is_open())
        throw(Exception("Failed to open file for reading: " + j2k_out_path_.native() + ". Did you call compress()?"));


    j2k_ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit);
    
    
    std::shared_ptr<dsl::protobuf::J2KMainHeader> header(new dsl::protobuf::J2KMainHeader);
    header->set_image_id(cfg_.image_id);

    // total (including fragments)
    int cumulative_tile_size = calculate_header_tile_size(*header);
    // previous complete packet
    int previous_cumulative_tile_size = cumulative_tile_size;
    
    if(cfg_.image_id > dsl::protobuf::J2KPacketFragmentIdentifier::descriptor()->FindFieldByName("image_id")->options().GetExtension(dccl::field).max())
        throw(Exception("Exceeded J2KPacketFragmentIdentifier image_id max"));

    
    std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>> fragments;

    bool eoc_found = false;
    
    try
    {
        // read SOC
        std::array<char, J2KMarker::MARKER_SIZE> soc_marker;
        
        j2k_ifs.read(soc_marker.data(), J2KMarker::MARKER_SIZE);
        
        using marker_type = std::uint16_t;
        if(to_big_endian<marker_type>(soc_marker) != J2KMarker::SOC)
            throw(Exception("First two bytes of J2K file were not SOC"));

        // read SIZ
        std::array<char, J2KMarker::MARKER_SIZE> siz_marker;
        j2k_ifs.read(siz_marker.data(), J2KMarker::MARKER_SIZE);

        if(to_big_endian<marker_type>(siz_marker) != J2KMarker::SIZ)
            throw(Exception("Two bytes of J2K file after SOC were not SIZ"));

        header->mutable_siz()->set_marker(to_big_endian<std::uint16_t>(siz_marker));
        read_j2k_marker_segment(j2k_ifs, header->mutable_siz());

        if(header->sot().psot() > dsl::protobuf::J2KMainHeader::SOT::descriptor()->FindFieldByName("psot")->options().GetExtension(dccl::field).max())
            throw(Exception("Exceeded J2KMainHeader::SOT::psot max: Use a smaller (compressed) image."));

        
        // read next marker
        bool qcd_found = false;
        bool sot_found = false;
        bool cod_found = false;
        
        while(!(qcd_found && sot_found && cod_found))
        {
            std::array<char, J2KMarker::MARKER_SIZE> next_marker;
            j2k_ifs.read(next_marker.data(), J2KMarker::MARKER_SIZE);

            switch(to_big_endian<marker_type>(next_marker))
            {
                case J2KMarker::QCD:
                    header->mutable_qcd()->set_marker(to_big_endian<marker_type>(next_marker));
                    read_j2k_marker_segment(j2k_ifs, header->mutable_qcd());
                    qcd_found = true;
                    break;
                    
                case J2KMarker::SOT:
                    header->mutable_sot()->set_marker(to_big_endian<marker_type>(next_marker));
                    read_j2k_marker_segment(j2k_ifs, header->mutable_sot());
                    sot_found = true;
                    break;

                case J2KMarker::COD:
                    header->mutable_cod()->set_marker(to_big_endian<marker_type>(next_marker));
                    read_j2k_marker_segment(j2k_ifs, header->mutable_cod());
                    cod_found = true;
                    break;

                case J2KMarker::COM:
                    header->mutable_com()->set_marker(to_big_endian<marker_type>(next_marker));
                    read_j2k_marker_segment(j2k_ifs, header->mutable_com());
                    break;
                    
                default:
                    constexpr int length_size = 2;
                    std::array<char, length_size> length_arr;
                    j2k_ifs.read(length_arr.data(), length_size);
                    auto length = to_big_endian<unsigned>(length_arr) - length_size;
                    j2k_ifs.seekg(length, std::ios_base::cur);
                    break;
            }
        }

        // read fragments
        while(!eoc_found)
        {
            fragments.emplace_back(new dsl::protobuf::J2KPacketFragment);
            auto& first_fragment = fragments.back();
            first_fragment->mutable_id()->set_image_id(cfg_.image_id);
            auto fragment_id = 0;
            first_fragment->mutable_id()->set_fragment_id(fragment_id++);
            cumulative_tile_size += fragment_header_size();
            
            std::array<char, J2KMarker::MARKER_SIZE> sop_marker;
            j2k_ifs.read(sop_marker.data(), J2KMarker::MARKER_SIZE);

            if(to_big_endian<marker_type>(sop_marker) != J2KMarker::SOP)
            {
                
                std::cerr << "Next word is not the start of packet (SOP) marker. Seeking until next SOP marker" << std::endl;

                // read until start of next marker
                do
                {
                    while(j2k_ifs.get() != 0xFF)
                    {
                        // keep reading
                    }
                } while(j2k_ifs.get() != (J2KMarker::SOP & 0xFF));
            }
            

            // read length
            constexpr int length_size = 2;
            std::array<char, length_size> length_arr;
            j2k_ifs.read(length_arr.data(), length_size);

            // read packet seq number
            constexpr int seq_size = 2;
            std::array<char, seq_size> seq_arr;
            j2k_ifs.read(seq_arr.data(), seq_size);
            auto packet_id = to_big_endian<std::uint16_t>(seq_arr);
            first_fragment->mutable_id()->set_packet_id(packet_id);            
            first_fragment->mutable_id()->set_is_last_fragment(false);

            // read until start of next marker
            std::string packet;
            do
            {
                std::string chunk;
                std::getline(j2k_ifs, chunk, static_cast<char>(0xFF));
                packet += chunk + std::string(1, 0xFF);
            }
            while(j2k_ifs.peek() != (J2KMarker::SOP & 0xFF) && 
                  j2k_ifs.peek() != (J2KMarker::EOC & 0xFF));

            if(j2k_ifs.peek() == (J2KMarker::EOC & 0xFF))
               eoc_found = true;

            // remove final 0xFF
            packet.pop_back();

            // add to the correct fragments
            auto frag_max_size = dsl::protobuf::J2KPacketFragment::descriptor()->FindFieldByName("data")->options().GetExtension(dccl::field).max_length();


            if(packet.size() <= frag_max_size)
            {
                first_fragment->set_data(packet);
                cumulative_tile_size += packet.size();
                first_fragment->set_cumulative_tile_size(previous_cumulative_tile_size);
            }
            else
            {
                first_fragment->set_data(packet.substr(0, frag_max_size));
                cumulative_tile_size += frag_max_size;
                first_fragment->set_cumulative_tile_size(previous_cumulative_tile_size);

                for(int i = 1, n = (packet.size() + frag_max_size - 1) / frag_max_size; i < n; ++i)
                {
                    fragments.emplace_back(new dsl::protobuf::J2KPacketFragment);
                    first_fragment->mutable_id()->set_is_last_fragment(false);
                    auto& next_fragment = fragments.back();
                    *next_fragment->mutable_id() = first_fragment->id();
                    next_fragment->mutable_id()->set_fragment_id(fragment_id++);
                    next_fragment->set_data(packet.substr(frag_max_size*i, frag_max_size));
                    cumulative_tile_size += next_fragment->data().size();
                    next_fragment->set_cumulative_tile_size(previous_cumulative_tile_size);
                }
            }

            previous_cumulative_tile_size = cumulative_tile_size;
            fragments.back()->mutable_id()->set_is_last_fragment(true);
            fragments.back()->set_cumulative_tile_size(cumulative_tile_size);
            
            // seek back to start of next SOP
            j2k_ifs.putback(static_cast<char>(0xFF));            

            // if the packet is empty, no need to send any bytes
            if(first_fragment->data().size() == 1 && !(first_fragment->data().at(0) & 0x80))
                first_fragment->clear_data();

            if(packet_id > dsl::protobuf::J2KPacketFragmentIdentifier::descriptor()->FindFieldByName("packet_id")->options().GetExtension(dccl::field).max())
                throw(Exception("Exceeded J2KPacketFragmentIdentifier packet_id max"));

            if(fragment_id > dsl::protobuf::J2KPacketFragmentIdentifier::descriptor()->FindFieldByName("fragment_id")->options().GetExtension(dccl::field).max())
                throw(Exception("Exceeded J2KPacketFragmentIdentifier fragment_id max"));
        }
    }
    catch (std::ifstream::failure e)
    {
        if(!j2k_ifs.eof())
        {
            throw(Exception(std::string("Error reading file: ") + e.what()));
        }
    }
    
    if(!eoc_found)
    {
        throw(Exception(std::string("EOF reached before EOC marker found.")));
    }


    return std::make_tuple(header, fragments);
}

void dsl::progressive_imagery::ProgressiveCompressor::read_j2k_marker_segment(
    std::ifstream& j2k_ifs, google::protobuf::Message* seg)
{    
    const auto* desc = seg->GetDescriptor();
    const auto* refl = seg->GetReflection();

    auto start_pos = j2k_ifs.tellg();
    
    // for fields with bits < 8
    char carry_byte = 0;
    bool carry_byte_set = false;

    decltype(start_pos) seg_length = 0;
    
    for(int f = 0, n = desc->field_count(); f < n; ++f)
    {
        const auto* field_desc = desc->field(f);

        if(field_desc->name() == "marker")
            continue;
        
        const auto& dsl_options = field_desc->options().GetExtension(dsl::field);        
        const auto& dccl_options = field_desc->options().GetExtension(dccl::field);

        bool dccl_omit_check = dccl_options.omit() && field_desc->has_default_value();
        
        auto num_bits = dsl_options.num_bits();
        auto num_bytes = intceil(num_bits, 8u);
        std::vector<char> bytes(num_bytes);
        bool not_whole_byte = num_bytes*8 != num_bits;
        if(not_whole_byte)
        {
            auto carry_bits = num_bits % 8;
            // read a new byte if necessary
            if(!carry_byte_set)
                j2k_ifs.read(&carry_byte, 1);

            // next time, use the rest of the byte
            // or read a new byte
            carry_byte_set = !carry_byte_set;

            // add the bits to the front of the bytes vector
            bytes.push_back(carry_byte >> (8-carry_bits));

            // remove the used bits
            carry_byte <<= carry_bits;
            
            
            --num_bytes;
        }

        if(field_desc->is_repeated())
        {
            // repeated is always the last field to the end of the segment
            while(j2k_ifs.tellg() < start_pos + seg_length)
            {
                switch(field_desc->cpp_type())
                {
                    case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
                        read_j2k_marker_segment(j2k_ifs, refl->AddMessage(seg, field_desc));
                        break;
                    case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                    {
                        j2k_ifs.read(bytes.data(), num_bytes);
                        auto value = to_big_endian<std::uint32_t>(bytes);
                        
                        if(dccl_omit_check && value != field_desc->default_value_uint32())
                            throw_non_matching_default_value(field_desc, field_desc->default_value_uint32(), value);
                            
                        refl->AddUInt32(seg, field_desc, value);
                        break;
                    }
                    default:
                    {
                        bool unsupported_protobuf_type = false;
                        assert(unsupported_protobuf_type);
                        break;
                    }
                }
            }
        }
        else
        {
            switch(field_desc->cpp_type())
            {
                case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                {
                    j2k_ifs.read(bytes.data(), num_bytes);
                    auto value = to_big_endian<std::uint32_t>(bytes);

                    if(dccl_omit_check && value != field_desc->default_value_uint32())
                        throw_non_matching_default_value(field_desc, field_desc->default_value_uint32(), value);

                    refl->SetUInt32(seg, field_desc, value);

                    if(field_desc->name() == "length")
                        seg_length = value;
                    break;
                }
                
                case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
                {
                    j2k_ifs.read(bytes.data(), num_bytes);

                    bool value = bytes[0] ? true : false;
                    
                    if(dccl_omit_check && value != field_desc->default_value_bool())
                        throw_non_matching_default_value(field_desc, field_desc->default_value_bool(), value);
                    
                    refl->SetBool(seg, field_desc, value);
                    break;
                }

                case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
                {
                    j2k_ifs.read(bytes.data(), num_bytes);
                    auto value = std::string(bytes.begin(), bytes.end());
                    
                    if(dccl_omit_check && value != field_desc->default_value_string())
                        throw_non_matching_default_value(field_desc, field_desc->default_value_string(), value);
                    
                    refl->SetString(seg, field_desc, value);
                    break;
                }
                

                case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
                {
                    j2k_ifs.read(bytes.data(), num_bytes);
                    const auto* enum_desc = field_desc->enum_type();
                    int value = to_big_endian<int>(bytes);

                    if(dccl_omit_check && value != field_desc->default_value_enum()->number())
                        throw_non_matching_default_value(field_desc, field_desc->default_value_enum()->number(), value);
                    
                    const auto* enum_val_desc = enum_desc->FindValueByNumber(value);
                        
                    if(enum_val_desc)
                        refl->SetEnum(seg, field_desc, enum_val_desc);
                    else
                        throw(Exception(std::string("Invalid enumeration value ") + std::to_string(value) + " for field: " + field_desc->DebugString()));
                    break;
                }
                    
                default:
                {
                    bool unsupported_protobuf_type = false;
                    assert(unsupported_protobuf_type);
                    break;
                }
            }
        }
    }
}


//
// DECOMPRESSOR
//

dpi::ProgressiveDecompressor::ProgressiveDecompressor(ProgressiveDecompressorConfiguration cfg, dsl::protobuf::J2KMainHeader header) try :
   cfg_(std::move(cfg)),
   header_(std::move(header)),
       image_(nullptr, opj_image_destroy),
       available_psot_size_(calculate_header_tile_size(header_))
{
    populate_header();    

    image_out_path_ = cfg_.common.out_dir;

    std::stringstream output_file;
    output_file << cfg_.filename_prefix << std::setfill('0') << std::setw(5) << header_.image_id();
    image_out_path_ /= output_file.str();
    j2k_out_path_ = image_out_path_;
    j2k_out_path_ += ".j2k";
    
    switch(cfg_.common.type)
    {
        case ImageType::TIFF:
            image_out_path_ += ".tif";
            break;

        case ImageType::PNG:
            image_out_path_ += ".png";
            break;

        case ImageType::PPM:
            image_out_path_ += ".ppm";
            break;

    }
    
    {
        // open/create the file to check that it is writable now, rather than waiting for decompress()
        std::ofstream fout(image_out_path_.c_str());
        if(!fout.is_open())
            throw(Exception("Failed to open file for writing: " + image_out_path_.native()));
        
        boost::filesystem::remove(image_out_path_);
    }


    opj_set_default_decoder_parameters(&parameters_);

}
catch (const boost::filesystem::filesystem_error& e)
{
    throw(dpi::Exception(e));
}

dpi::ProgressiveDecompressor::~ProgressiveDecompressor()
{
}

void dpi::ProgressiveDecompressor::populate_header()
{
    auto& siz = *header_.mutable_siz();
    // from the JPEG2000 standard
    siz.set_length(38+3*siz.csiz());

    // tile size = image size since we're using 1 tile
    siz.set_xtsiz(siz.xsiz());
    siz.set_ytsiz(siz.ysiz());
}

float dpi::ProgressiveDecompressor::decompress()
{
    ImageRemover image_remover(j2k_out_path_);
    try
    {
        reassemble_j2k();
    }
    catch(Exception& e)
    {
        PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                             "id: " << image_id() << "Failed to reassemble J2K");
        return 0;
    }
    image_remover.commit();
    
    decode();
    
    // partial image
    float fractional_data = (static_cast<float>(available_psot_size_))/header_.sot().psot();
    if(available_psot_size_ != header_.sot().psot())
    {

        PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                             "id: " << image_id() << ": have " << fractional_data*100 << "% of the image by data size (" << available_psot_size_ << "/" << header_.sot().psot() << "B)");
        
        if(cfg_.rename_partial)
        {
            boost::filesystem::path partial_image_out_path_ = cfg_.common.out_dir;
            std::stringstream partial_filename;
            partial_filename << image_out_path_.stem().native() << "_" << std::setfill('0') << std::setw(2) << static_cast<int>(fractional_data*100) << "pct" << "_" << std::setfill('0') << std::setw(7) << available_psot_size_ << "B" << image_out_path_.extension().native();
            partial_image_out_path_ /= partial_filename.str().c_str();
            boost::filesystem::rename(image_out_path_, partial_image_out_path_);
        }  
    }
    return fractional_data;
}

void dpi::ProgressiveDecompressor::reassemble_j2k()
{
    //
    // Reassemble header and fragments into J2K file
    //
    std::ofstream j2k_ofs(j2k_out_path_.c_str(), std::ios::out | std::ios::binary);

    auto soc_bytes = from_big_endian<std::vector<char>>(J2KMarker::SOC, J2KMarker::MARKER_SIZE);

    j2k_ofs.write(soc_bytes.data(), J2KMarker::MARKER_SIZE);
    
    write_j2k_marker_segment(j2k_ofs, header_);

    // start with the sot marker segment and add on fragments
    available_psot_size_ = calculate_header_tile_size(header_);
    
    dsl::protobuf::J2KPacketFragmentIdentifier previous_id;
    // write all fragments received that are in order
    bool all_fragments_written = true;
    bool no_packets_written = true;
    for(auto it = fragments_.begin(), end = fragments_.end(); it != end; ++it)
    {
        const auto& fragment = *it;
        
        if((!previous_id.IsInitialized() &&
            fragment.id().packet_id() == 0 &&
            fragment.id().fragment_id() == 0) || // first packet, first fragment
           (previous_id.is_last_fragment() &&
            fragment.id().packet_id() == previous_id.packet_id() + 1 &&
            fragment.id().fragment_id() == 0)) // this is the start of the next packet
        {
            // check forward to ensure we have a complete packet before writing any fragments
            bool have_complete_packet = fragment.id().is_last_fragment();
            auto packet_it = it;
            auto previous_packet_it = packet_it++;
            while(!have_complete_packet && packet_it != end)
            {
                if(packet_it->id().packet_id() == previous_packet_it->id().packet_id() &&
                   packet_it->id().fragment_id() == previous_packet_it->id().fragment_id() + 1)
                    have_complete_packet = packet_it->id().is_last_fragment();
                else
                    break;
                
                previous_packet_it = packet_it++;
            }
            
            if(!have_complete_packet)
            {
                all_fragments_written = false;
                break;
            }

            no_packets_written = false;
            
            auto sop_bytes = from_big_endian<std::vector<char>>(J2KMarker::SOP, J2KMarker::MARKER_SIZE);
            j2k_ofs.write(sop_bytes.data(), J2KMarker::MARKER_SIZE);
                
            const int length_size = 2;
            const int length = 0x04;
            auto length_bytes = from_big_endian<std::vector<char>>(length, length_size);
            j2k_ofs.write(length_bytes.data(), length_size);
            
            const int nsop_size = 2;
            const int nsop = fragment.id().packet_id();
            auto nsop_bytes = from_big_endian<std::vector<char>>(nsop, nsop_size);
            j2k_ofs.write(nsop_bytes.data(), nsop_size);

            available_psot_size_ += fragment_header_size();
            
            if(fragment.has_data())
            {
                j2k_ofs.write(fragment.data().data(), fragment.data().size());
                available_psot_size_ += fragment.data().size();
            }
            else
            {
                j2k_ofs.put(0x00);
                available_psot_size_ += 1;
            }
            
        }
        else if(fragment.id().packet_id() == previous_id.packet_id() &&
                fragment.id().fragment_id() == previous_id.fragment_id() + 1) // this is the next fragment
        {
            j2k_ofs.write(fragment.data().data(), fragment.data().size());
            available_psot_size_ += fragment.data().size();
        } 
        else
        {
            all_fragments_written = false;
            break;
        }
        previous_id = fragment.id();
    }

    if(!all_fragments_written)
    {
        PROG_IMAGERY_CONSOLE(cfg_.common.warning_callback,
                             "Stopped writing at fragment (" << previous_id.ShortDebugString() << ") since the next fragment is not sequential or we do not have a complete packet");
    }
    
    if(no_packets_written)
        throw(Exception("Empty image - no full packets yet."));

    {       
        // determine how many resolutions and layers we have

        // total precincts, components, layers, resolutions 
        const int p_t = 1, c_t = header_.siz().csiz(), l_t = header_.cod().sgcod_number_of_layers(), r_t = (header_.cod().spcod_number_of_decomp_levels()+1);
        
        int total_expected_packets = p_t*c_t*l_t*r_t;
        int total_available_packets = previous_id.packet_id() + 1;

        // available precincts, components, layers, resolutions 
        int p_a = 0, c_a = 0, l_a = 0, r_a = 0;
        switch(header_.cod().sgcod_progression_order())
        {
            case dsl::protobuf::J2KMainHeader::COD::LAYER_RESOLUTION_COMPONENT_PRECINCT:
                l_a = intceil(total_available_packets, r_t*c_t*p_t);
                r_a = std::min(r_t, intceil(total_available_packets, c_t*p_t));
                c_a = std::min(c_t, intceil(total_available_packets, p_t));
                p_a = std::min(p_t, total_available_packets);
                break;
                
            case dsl::protobuf::J2KMainHeader::COD::RESOLUTION_LAYER_COMPONENT_PRECINCT:
                r_a = intceil(total_available_packets, l_t*c_t*p_t);
                l_a = std::min(l_t, intceil(total_available_packets, c_t*p_t));
                c_a = std::min(c_t, intceil(total_available_packets, p_t));
                p_a = std::min(p_t, total_available_packets);
                break;

            default:
                bool unsupported_progression_order = false;
                assert(unsupported_progression_order);
                break;
        }

        PROG_IMAGERY_CONSOLE(cfg_.common.info_callback,
                             "Available (l, r, c, p): " << "(" << l_a << ", " << r_a << ", " << c_a << ", " << p_a << ")");        
        

        parameters_.cp_layer = l_a;
        parameters_.cp_reduce = r_t - r_a;
    }
    
    
    // overwrite PSOT tile size in bytes with current value
    j2k_ofs.seekp(psot_pos_);
    constexpr int psot_field_size = 4;
    auto psot_bytes = from_big_endian<std::vector<char>>(available_psot_size_, psot_field_size);
    j2k_ofs.write(psot_bytes.data(), psot_field_size);
    j2k_ofs.seekp(0, std::ios_base::end);
    
    auto eoc_bytes = from_big_endian<std::vector<char>>(J2KMarker::EOC, J2KMarker::MARKER_SIZE);
    j2k_ofs.write(eoc_bytes.data(), J2KMarker::MARKER_SIZE);
}

void dpi::ProgressiveDecompressor::decode()
{    
    //
    // perform the decoding
    //

    std::unique_ptr<opj_stream_t, decltype(&opj_stream_destroy)> stream(
        opj_stream_create_default_file_stream(j2k_out_path_.c_str(), OPJ_TRUE),
        opj_stream_destroy);

    if(!stream)
        throw(Exception("Failed to create stream: opj_create_create_default_file_stream"));

    std::unique_ptr<opj_codec_t, decltype(&opj_destroy_codec)> codec(
        opj_create_decompress(OPJ_CODEC_J2K),
        opj_destroy_codec);

    if(!codec)
        throw(Exception("Failed to create codec: opj_create_decompress"));    

    opj_set_info_handler(codec.get(), info_callback, &cfg_.common.info_callback);
    opj_set_warning_handler(codec.get(), warning_callback, &cfg_.common.warning_callback);
    opj_set_error_handler(codec.get(), error_callback, &cfg_.common.error_callback);
    
    if(!opj_setup_decoder(codec.get(), &parameters_))
        throw(Exception("Failed to decode image: opj_setup_decoder"));

    ImageRemover image_remover(image_out_path_);

    opj_image_t* image = NULL;
    if(!opj_read_header(stream.get(), codec.get(), &image))
        throw(Exception("Failed to decode image: opj_read_header"));
    image_.reset(image);
    
    if(!opj_decode(codec.get(), stream.get(), image_.get()))
        throw(Exception("Failed to decode image: opj_decode"));
    
    if (image_->comps[0].data == NULL)
        throw(Exception("Failed to decode image: no image data"));

    switch(cfg_.common.type)
    {
        case ImageType::TIFF:            
            if(imagetotif(image_.get(), image_out_path_.c_str()))
                throw(Exception("Failed to decode image: imagetotif"));
            break;
        case ImageType::PNG:   
            if(imagetopng(image_.get(), image_out_path_.c_str()))
                throw(Exception("Failed to decode image: imagetopng"));
            break;
        case ImageType::PPM:
        {
            bool force_split_comp = false;
            if(imagetopnm(image_.get(), image_out_path_.c_str(), force_split_comp))
                throw(Exception("Failed to decode image: imagetopnm"));
            break;
        }
        
    }
    
    // we have finished decompression successfully, inform image_remover not to delete the image
    image_remover.commit();
}



void dpi::ProgressiveDecompressor::write_j2k_marker_segment(std::ofstream& j2k_ofs, const google::protobuf::Message& seg)
{
    const auto* desc = seg.GetDescriptor();
    const auto* refl = seg.GetReflection();
    
    // for fields with bits < 8
    char carry_byte = 0;
    int carry_byte_set_bits = 0;
    
    for(int f = 0, n = desc->field_count(); f < n; ++f)
    {
        const auto* field_desc = desc->field(f);

        const auto& dsl_options = field_desc->options().GetExtension(dsl::field);        
        const auto& dccl_options = field_desc->options().GetExtension(dccl::field);

        if(field_desc->name() == "psot")
            psot_pos_ = j2k_ofs.tellp();
        
        auto num_bits = dsl_options.num_bits();
        auto num_bytes = intceil(num_bits, 8u);
        std::vector<char> bytes(num_bytes);
        bool not_whole_byte = num_bytes*8 != num_bits;
 
        if(field_desc->is_repeated())
        {
            for(int i = 0, n = refl->FieldSize(seg, field_desc); i < n; ++i)
            {
                switch(field_desc->cpp_type())
                {
                    case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
                        write_j2k_marker_segment(j2k_ofs, refl->GetRepeatedMessage(seg, field_desc, i));
                        break;
                    case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                    {
                        auto bytes = from_big_endian<std::vector<char>>(refl->GetRepeatedUInt32(seg, field_desc, i), num_bytes);
                        j2k_ofs.write(bytes.data(), num_bytes);
                        break;
                    }
                    default:
                    {
                        bool unsupported_protobuf_type = false;
                        assert(unsupported_protobuf_type);
                        break;
                    }

                }
            }
        }
        else
        {
            switch(field_desc->cpp_type())
            {

                case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
                    write_j2k_marker_segment(j2k_ofs, refl->GetMessage(seg, field_desc));
                    break;
                    
                case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                {
                    auto bytes = from_big_endian<std::vector<char>>(refl->GetUInt32(seg, field_desc), num_bytes);
                    if(not_whole_byte)
                    {
                        carry_byte <<= num_bits;
                        carry_byte |= bytes[0];
                        carry_byte_set_bits += num_bits;
                    }
                    else
                    {
                       j2k_ofs.write(bytes.data(), num_bytes);
                    }
                    break;
                }
                
                case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
                {
                    auto bytes = from_big_endian<std::vector<char>>(refl->GetBool(seg, field_desc), num_bytes);
                    if(not_whole_byte)
                    {
                        carry_byte <<= num_bits;
                        carry_byte |= bytes[0];
                        carry_byte_set_bits += num_bits;
                    }
                    else
                    {
                        j2k_ofs.write(bytes.data(), num_bytes);
                    }
                    
                    break;
                }
                

                case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
                {
                    auto bytes = from_big_endian<std::vector<char>>(refl->GetEnum(seg, field_desc)->number(), num_bytes);
                    j2k_ofs.write(bytes.data(), num_bytes);
                    break;
                }


                case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
                {
                    auto bytes = refl->GetString(seg, field_desc);
                    j2k_ofs.write(bytes.data(), num_bytes);
                    break;
                }
                
                
                default:
                {
                    bool unsupported_protobuf_type = false;
                    assert(unsupported_protobuf_type);
                    break;
                }
            }
        }

        if(carry_byte_set_bits == 8)
        {
            j2k_ofs.write(&carry_byte, 1);
            carry_byte_set_bits = 0;
        }
    }
}
