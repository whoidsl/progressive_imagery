#include "ros/ros.h"

#include <boost/asio.hpp>
#include "ds_acomms_msgs/AcousticModemData.h"
#include "ds_acomms_msgs/ModemData.h"
#include "ds_acomms_msgs/ModemDataRequest.h"
#include "ds_acomms_msgs/AcksExpected.h"

namespace dsl
{
    namespace progressive_imagery
    {
        namespace ros
        {
            struct ModemDriverSimConfiguration
            {
                int local_port;
                int remote_port;
                std::string remote_ip_addr { "127.0.0.1" };

                int num_frames { 1 };
                int frame_size { 256 };
                int tx_period_ms { 1000 };
            };

            class ModemDriverSim
            {
            public:
                ModemDriverSim();
                void loop();

            private:
                ros::ModemDriverSimConfiguration create_driver_configuration();
                void publish_transfer_progress();
                void async_tx_timer_wait();
                void request_data();
                void start_receive();

            private:
                ::ros::NodeHandle node_handle_;
                ros::ModemDriverSimConfiguration driver_cfg_;
                ::ros::ServiceClient data_client_;
                ::ros::Publisher acks_pub_;
                ::ros::Publisher rx_data_pub_;

                boost::asio::io_service io_;
                boost::asio::deadline_timer tx_timer_;
                boost::posix_time::ptime expire_time_;

                boost::asio::ip::udp::socket udp_;
                boost::asio::ip::udp::endpoint remote_endpoint_;
                boost::asio::ip::udp::endpoint sender_endpoint_;

                static constexpr int UDP_MAX { 65507 };
                std::array<unsigned char, UDP_MAX> rx_data_;
            };
        }
    }
}

namespace dpi = dsl::progressive_imagery;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "progressive_imagery_udp_modemdriver_sim");

    dpi::ros::ModemDriverSim driver;

    ros::Rate loop_rate(10);
    while(ros::ok())
    {
        driver.loop();
        ros::spinOnce();
        loop_rate.sleep();
    }
}

dpi::ros::ModemDriverSim::ModemDriverSim() :
    node_handle_("~"),
    driver_cfg_(create_driver_configuration()),
    data_client_(node_handle_.serviceClient<::ds_acomms_msgs::ModemDataRequest>("outgoing_data")),
    acks_pub_(node_handle_.advertise<::ds_acomms_msgs::AcksExpected>("acks_expected", 1)),
    // rx_data_pub_(node_handle_.advertise<::progressive_imagery::AcousticModemData>("incoming_data", 10)),
    rx_data_pub_(node_handle_.advertise<::ds_acomms_msgs::ModemData>("incoming_data", 10)),
    tx_timer_(io_),
    expire_time_(boost::posix_time::second_clock::universal_time() + boost::posix_time::seconds(1)),
    udp_(io_, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(),
                                             driver_cfg_.local_port)),
    remote_endpoint_(boost::asio::ip::address::from_string(driver_cfg_.remote_ip_addr),
                     driver_cfg_.remote_port)
{
    tx_timer_.expires_at(expire_time_);
    async_tx_timer_wait();

    start_receive();
}


void dpi::ros::ModemDriverSim::loop()
{
    io_.poll();
}

void dpi::ros::ModemDriverSim::async_tx_timer_wait()
{
    tx_timer_.async_wait(
        [this](const boost::system::error_code& error)
        {
            if(!error)
            {
                request_data();

                expire_time_ += boost::posix_time::milliseconds(driver_cfg_.tx_period_ms);
                tx_timer_.expires_at(expire_time_);
                async_tx_timer_wait();
            }
            else
            {
                ROS_FATAL_STREAM("Timer error" << error.message());
            }
        }
        );
}

void dpi::ros::ModemDriverSim::request_data()
{
  ::ds_acomms_msgs::ModemDataRequest data_srv;
  ::ds_acomms_msgs::AcksExpected acks_msg;
  data_srv.request.bytes_requested = driver_cfg_.frame_size;
  // Replacing this with acks
  //data_srv.request.reset_unacknowledged = true;

    for(int i = 0; i < driver_cfg_.num_frames; ++i)
    {
      //if(i != 0 ) { // no chance to ack yet, so don't request any unack'd frames
	//data_srv.request.reset_unacknowledged = false;
	//}
      if (i == 0) { // First frame, so we should clear acks.
	acks_pub_.publish(acks_msg);
      }
        if(data_client_.call(data_srv))
        {
	  //ROS_INFO_STREAM("Received: " << data_srv.response.packet.stamp << ", " << data_srv.response.packet.payload.size());

            // send the data over UDP
            udp_.async_send_to(
			       boost::asio::buffer(data_srv.response.data),
                remote_endpoint_,
                [this](const boost::system::error_code& ec,
                   std::size_t bytes_transferred)
                {
                    if(!ec)
                    {
                        ROS_INFO_STREAM("UDP sent " << bytes_transferred << "B to " << remote_endpoint_);
                    }
                    else
                    {
                        ROS_FATAL_STREAM("Failed to write UDP packet" << ec.message());
                    }
                });
        }
        else
        {
            ROS_ERROR("Failed to call outgoing_data service");
        }
    }
}


void dpi::ros::ModemDriverSim::start_receive()
{
    udp_.async_receive_from(
        boost::asio::buffer(rx_data_),
        sender_endpoint_,
        [this](const boost::system::error_code& ec,
               std::size_t bytes_transferred)
        {
            if(!ec)
            {
                ROS_INFO_STREAM("UDP receive " << bytes_transferred << "B from " << sender_endpoint_);
		::ds_acomms_msgs::AcousticModemData rx_data;
                rx_data.payload.assign(rx_data_.begin(), rx_data_.begin()+bytes_transferred);
                rx_data.stamp = ::ros::Time::now();
                rx_data.local_addr = driver_cfg_.local_port;
                rx_data.remote_addr = sender_endpoint_.port();
                rx_data_pub_.publish(rx_data);

                start_receive();
            }
            else
            {
                ROS_FATAL_STREAM("Failed to received UDP data" << ec.message());
            }
        });
}


#define SET_CONFIG(_NAME)                                               \
    node_handle_.getParam(#_NAME, cfg._NAME);                           \
    ROS_INFO_STREAM("Parameter " #_NAME " set to " << cfg._NAME);

dpi::ros::ModemDriverSimConfiguration dpi::ros::ModemDriverSim::create_driver_configuration()
{
    dpi::ros::ModemDriverSimConfiguration cfg;

    SET_CONFIG(local_port);
    SET_CONFIG(remote_port);
    SET_CONFIG(remote_ip_addr);
    SET_CONFIG(num_frames);
    SET_CONFIG(frame_size);
    SET_CONFIG(tx_period_ms);

    return cfg;
}

#undef SET_CONFIG
