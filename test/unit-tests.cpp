#include "progressive_imagery/preprocess.h"
#include "progressive_imagery/progressive_codec.h"
#include "progressive_imagery/manager.h"
#include <boost/filesystem.hpp>
#include <dccl.h>

#include <gtest/gtest.h>

namespace dpi = dsl::progressive_imagery;


TEST(BasicTests, J2KPacketFragmentOperators)
{
    auto create_fragments =
        [](std::pair<int,int> i_id, std::pair<int,int> p_id, std::pair<int,int> f_id)->std::pair<dsl::protobuf::J2KPacketFragment, dsl::protobuf::J2KPacketFragment>
        {
            dsl::protobuf::J2KPacketFragment f1;
            dsl::protobuf::J2KPacketFragment f2;
            f1.mutable_id()->set_image_id(i_id.first);
            f2.mutable_id()->set_image_id(i_id.second);
            f1.mutable_id()->set_packet_id(p_id.first);
            f2.mutable_id()->set_packet_id(p_id.second);
            f1.mutable_id()->set_fragment_id(f_id.first);
            f2.mutable_id()->set_fragment_id(f_id.second);
            return std::make_pair(f1, f2);
        };

    {
        auto fp = create_fragments({ 0, 0 }, { 1, 2 }, { 4, 4 });
        ASSERT_LT(fp.first, fp.second);
    }
    {
        auto fp = create_fragments({ 1, 0 }, { 1, 2 }, { 4, 4 });
        ASSERT_LT(fp.second, fp.first);
    }
    {
        auto fp = create_fragments({ 0, 0 }, { 1, 1 }, { 40, 4 });
        ASSERT_LT(fp.second, fp.first);
    }

    {
        auto fp = create_fragments({ 0, 0 }, { 1, 1 }, { 4, 4 });
        ASSERT_EQ(fp.second, fp.first);
    }

    using namespace std::rel_ops;
    {
        auto fp = create_fragments({ 0, 0 }, { 2, 1 }, { 4, 4 });
        ASSERT_TRUE(fp.second != fp.first);
    }

}



//
// Test DCCL Custom Codecs
//

class DCCLCustomCodecsTest : public ::testing::TestWithParam<int>
{
public:
    DCCLCustomCodecsTest()
        {
        }

    virtual void SetUp()
        {
            dccl_.load_library("libprogressive_imagery.so");
            dccl_.load<dsl::protobuf::J2KPacketFragment>();
            auto& id = *fragment_.mutable_id();
            id.set_image_id(0);
            id.set_packet_id(1);
            id.set_fragment_id(0);
            id.set_is_last_fragment(true);
            //            dccl::dlog.connect(dccl::logger::DEBUG2_PLUS, &std::cout);
        }

    virtual void TearDown()
        {
        }


    dccl::Codec dccl_;
    dsl::protobuf::J2KPacketFragment fragment_;

};



TEST_P(DCCLCustomCodecsTest, var_bytes)
{
    int data_size = GetParam();
    if(data_size >= 0)
    {
        fragment_.set_data("");
        for(int i = 0; i < data_size; ++i)
            fragment_.mutable_data()->push_back(i);
    }

    std::string encoded;
    dccl_.encode(&encoded, fragment_);
    decltype(fragment_) out_fragment;
    dccl_.decode(encoded, &out_fragment);

    if(data_size >= 0)
        ASSERT_EQ(8+data_size, encoded.size());
    else
        ASSERT_EQ(7, encoded.size());

    ASSERT_EQ(fragment_.SerializeAsString(), out_fragment.SerializeAsString());
}

INSTANTIATE_TEST_CASE_P(AllBytes,
                        DCCLCustomCodecsTest,
			//                        ::testing::Values(-1, 0, 20, 40, 55, 73, 112, 248));
                        ::testing::Values(-1, 0, 20, 40, 55, 73, 112, 232));

//
// Test PreProcessor
//

boost::filesystem::path make_pp_out_path(boost::filesystem::path image1_path)
{
    boost::filesystem::path image1_out_path = image1_path.parent_path();
    image1_out_path /= image1_path.stem();
    image1_out_path += ".out";
    image1_out_path += image1_path.extension();
    return image1_out_path;
}

class PreProcessorTest : public ::testing::Test
{
public:
    PreProcessorTest() :
        image1_path_("test/data/image1_1000w.tif")
        {
        }

    virtual void SetUp()
        {
            ASSERT_TRUE(boost::filesystem::exists(image1_path_));
            image1_out_path_ = make_pp_out_path(image1_path_);
        }

    virtual void TearDown()
        {
            boost::filesystem::remove(image1_out_path_);
        }


    boost::filesystem::path image1_path_;
    boost::filesystem::path image1_out_path_;

};



TEST_F(PreProcessorTest, resize_smaller)
{
    dpi::PreProcessor pp({ image1_path_.native(), image1_out_path_.native(), 500, 500, true });
    ASSERT_EQ(1000, pp.image().size().width());
    ASSERT_EQ(667, pp.image().size().height());

    pp.process();
    ASSERT_EQ(500, pp.image().size().width());
    ASSERT_EQ(334, pp.image().size().height());
    ASSERT_TRUE(pp.image().type() == Magick::GrayscaleType);

    pp.write();
    ASSERT_TRUE(boost::filesystem::exists(image1_out_path_));
}


TEST_F(PreProcessorTest, resize_larger)
{
    dpi::PreProcessor pp({ image1_path_.native(), image1_out_path_.native(), 2000, 2000, false });
    auto orig_type = pp.image().type();


    ASSERT_EQ(1000, pp.image().size().width());
    ASSERT_EQ(667, pp.image().size().height());
    pp.process();
    ASSERT_EQ(1000, pp.image().size().width());
    ASSERT_EQ(667, pp.image().size().height());
    ASSERT_TRUE(pp.image().type() == orig_type);

    pp.write();
    ASSERT_TRUE(boost::filesystem::exists(image1_out_path_));
}


//
// Test ProgressiveCompressor
//

class ProgressiveCompressorTest : public ::testing::Test
{
public:
    ProgressiveCompressorTest() :
        image1_path_("test/data/image1_1000w.tif")
        {
        }

    virtual void SetUp()
        {
            ASSERT_TRUE(boost::filesystem::exists(image1_path_));
            image1_out_dir_ = image1_path_.parent_path();
        }

    virtual void TearDown()
        {
        }


    boost::filesystem::path image1_path_;
    boost::filesystem::path image1_out_dir_;

};




TEST_F(ProgressiveCompressorTest, compress_defaults)
{
    dpi::ProgressiveCompressor pc({
            { image1_out_dir_.native() },
                0, image1_path_.native()
        });

    ASSERT_NO_THROW(pc.image());
    ASSERT_EQ(1000, pc.image().x1);
    ASSERT_EQ(667, pc.image().y1);
    ASSERT_EQ(3, pc.image().numcomps);

    ASSERT_EQ(1, pc.parameters().tcp_mct);
    ASSERT_TRUE(pc.parameters().prog_order == OPJ_LRCP);
    ASSERT_TRUE(pc.parameters().irreversible);
    ASSERT_EQ(6, pc.parameters().numresolution);
    ASSERT_TRUE(pc.parameters().csty & 0x02); // SOP
    ASSERT_EQ(10, pc.parameters().tcp_numlayers);


    // these are from calculate_opj.m
    ASSERT_FLOAT_EQ(3126.5625000000000, pc.parameters().tcp_rates[0]);
    ASSERT_FLOAT_EQ(2127.2974924600999, pc.parameters().tcp_rates[1]);
    ASSERT_FLOAT_EQ(1447.4025775678651, pc.parameters().tcp_rates[2]);
    ASSERT_FLOAT_EQ(984.8054768904843, pc.parameters().tcp_rates[3]);
    ASSERT_FLOAT_EQ(670.0567225347653, pc.parameters().tcp_rates[4]);
    ASSERT_FLOAT_EQ(455.9032437874628, pc.parameters().tcp_rates[5]);
    ASSERT_FLOAT_EQ(310.1942875965202, pc.parameters().tcp_rates[6]);
    ASSERT_FLOAT_EQ(211.0546423362799, pc.parameters().tcp_rates[7]);
    ASSERT_FLOAT_EQ(143.6005233907945, pc.parameters().tcp_rates[8]);
    ASSERT_FLOAT_EQ(97.7050781250000, pc.parameters().tcp_rates[9]);

    pc.compress();

    ASSERT_TRUE(boost::filesystem::exists(pc.j2k_out_path()));
    boost::filesystem::remove(pc.j2k_out_path());
}


TEST_F(ProgressiveCompressorTest, compress_unwriteable_dir)
{
    boost::filesystem::path unwriteable_dir("/tmp/progressive_image_test");
    boost::filesystem::create_directory(unwriteable_dir);
    boost::filesystem::permissions(unwriteable_dir, boost::filesystem::owner_read);

    ASSERT_THROW(dpi::ProgressiveCompressor pc({
                { unwriteable_dir.native() },
                 0, image1_path_.native()
                 }), dpi::Exception);

    boost::filesystem::remove(unwriteable_dir);
}

TEST_F(ProgressiveCompressorTest, compress_out_of_bounds)
{
    // resolution too large
    ASSERT_THROW(dpi::ProgressiveCompressor pc({
                { image1_out_dir_.native(),
                        dpi::ImageType::TIFF },
                    0, image1_path_.native(),
                        dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT,
                        11, // number_of_resolutions
                        10 // number_of_layers
                        }), dpi::Exception);

    // resolution too small
    ASSERT_THROW(dpi::ProgressiveCompressor pc({
                { image1_out_dir_.native(),
                        dpi::ImageType::TIFF },
                    0, image1_path_.native(),
                        dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT,
                        0, // number_of_resolutions
                        10 // number_of_layers
                        }), dpi::Exception);

    // layers too large
    ASSERT_THROW(dpi::ProgressiveCompressor pc({
                { image1_out_dir_.native(),
                        dpi::ImageType::TIFF
                        },
                    0, image1_path_.native(),
                        dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT,
                        6, // number_of_resolutions
                        33 // number_of_layers
                        }), dpi::Exception);

    // resolution too small
    ASSERT_THROW(dpi::ProgressiveCompressor pc({
                { image1_out_dir_.native(),
                        dpi::ImageType::TIFF },
                    0, image1_path_.native(),
                        dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT,
                        6, // number_of_resolutions
                        0 // number_of_layers
                        }), dpi::Exception);
}

TEST_F(ProgressiveCompressorTest, packetize)
{
    dpi::ProgressiveCompressor pc({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF
                    },
                0, image1_path_.native(),
                    dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT,
                    6, // number_of_resolutions
                    10 // number_of_layers
        });

    pc.compress();

    auto packets = pc.packetize();
    auto& header = std::get<0>(packets);
    auto& fragments = std::get<1>(packets);

    ASSERT_EQ(0xFF51, header->siz().marker());

    ASSERT_EQ(1000, header->siz().xsiz());
    ASSERT_EQ(667, header->siz().ysiz());

    ASSERT_EQ(3, header->siz().csiz());
    ASSERT_EQ(header->siz().csiz(), header->siz().comp_size());

    ASSERT_EQ((5+6*(pc.parameters().numresolution-1)-3)/2, header->qcd().spqcd_i_size());

    ASSERT_EQ(0x0a, header->sot().length());
    ASSERT_EQ(0xFF93, header->sot().sod_marker());

    ASSERT_EQ(dsl::protobuf::J2KMainHeader::COD::LAYER_RESOLUTION_COMPONENT_PRECINCT, header->cod().sgcod_progression_order());
    ASSERT_EQ(pc.parameters().tcp_numlayers, header->cod().sgcod_number_of_layers());
    ASSERT_EQ(pc.parameters().numresolution-1, header->cod().spcod_number_of_decomp_levels());

    header->set_image_id(0);

    int total_size = 0;
    int max_size = 0;
    for(const auto& fragment : fragments)
    {
        total_size += fragment->data().size();
        if(fragment->data().size() > max_size)
            max_size = fragment->data().size();
    }

    ASSERT_EQ(dsl::protobuf::J2KPacketFragment::descriptor()->FindFieldByName("data")->options().GetExtension(dccl::field).max_length(), max_size);

    ASSERT_TRUE(boost::filesystem::exists(pc.j2k_out_path()));
    boost::filesystem::remove(pc.j2k_out_path());
}



//
// Test ProgressiveDecompressor
//

class ProgressiveDecompressorTest :
    // image id, input path, progression order, grayscale
    public ::testing::TestWithParam<std::tuple<int, const char*, dsl::progressive_imagery::ProgressiveCompressorConfiguration::ProgressionOrder, bool>>
{
public:
    ProgressiveDecompressorTest() :
        image1_path_(std::get<1>(GetParam())),
        image1_out_dir_(image1_path_.parent_path()),
        image1_pp_out_path_(make_pp_out_path(image1_path_)),
        pp_({ image1_path_.native(), image1_pp_out_path_.native(), 50000, 50000, std::get<3>(GetParam()) }, true),
        pc_({ { image1_out_dir_.native(),
                        dpi::ImageType::TIFF,
                        [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                        [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
                },
                    std::get<0>(GetParam()), image1_pp_out_path_.native(),
                        std::get<2>(GetParam())
                        })
        {
            dccl_.load_library("libprogressive_imagery.so");
            dccl_.load<dsl::protobuf::J2KMainHeader>();
            dccl_.load<dsl::protobuf::J2KPacketFragment>();

        }

    virtual void SetUp()
        {
            pc_.compress();
            std::tie(orig_header_, orig_fragments_) = pc_.packetize();

            {
                // pass header through DCCL
                std::string encoded;
                dccl_.encode(&encoded, *orig_header_);
                dccl_.decode(encoded, header_.get());
            }

            // pass fragments through DCCL
            for(const auto& orig_fragment : orig_fragments_)
            {
                std::string encoded;
                dccl_.encode(&encoded, *orig_fragment);
                fragments_.emplace_back(new dsl::protobuf::J2KPacketFragment);
                dccl_.decode(encoded, fragments_.back().get());
                fragments_.back()->set_cumulative_tile_size(orig_fragment->cumulative_tile_size());
                ASSERT_EQ(orig_fragment->SerializeAsString(), fragments_.back()->SerializeAsString());
            }
        }

    virtual void TearDown()
        {
            boost::filesystem::remove(pc_.j2k_out_path());

        }


    boost::filesystem::path image1_path_;
    boost::filesystem::path image1_out_dir_;
    boost::filesystem::path image1_pp_out_path_;
    dpi::PreProcessor pp_;
    dpi::ProgressiveCompressor pc_;
    std::shared_ptr<dsl::protobuf::J2KMainHeader> orig_header_;
    std::shared_ptr<dsl::protobuf::J2KMainHeader> header_ { new dsl::protobuf::J2KMainHeader } ;
    std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>> orig_fragments_;
    std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>> fragments_;
    dccl::Codec dccl_;

};


TEST_P(ProgressiveDecompressorTest, verify_header_compatibility)
{
    dpi::ProgressiveDecompressor pd({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                    [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
            }},
        *header_);

    // instantiate all the default fields so we can directly compare the original header
    // with the DCCL + decompress populated header
    std::function<void(google::protobuf::Message&)> instantiate_defaults =
        [&instantiate_defaults](google::protobuf::Message& msg)
    {
        const auto* desc = msg.GetDescriptor();
        const auto* refl = msg.GetReflection();


        for(int f = 0, n = desc->field_count(); f < n; ++f)
        {
            const auto* field_desc = desc->field(f);

            if(field_desc->is_repeated())
            {
                switch(field_desc->cpp_type())
                {
                    case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
                        for(int i = 0; i < refl->FieldSize(msg, field_desc); ++i)
                            instantiate_defaults(*refl->MutableRepeatedMessage(&msg, field_desc, i));
                        break;
                    default: break;
                }
            }
            else
            {
                switch(field_desc->cpp_type())
                {
                    case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
                        instantiate_defaults(*refl->MutableMessage(&msg, field_desc));
                        break;

                    case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
                        if(field_desc->has_default_value())
                            refl->SetUInt32(&msg, field_desc, field_desc->default_value_uint32());
                       break;

                    case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
                        if(field_desc->has_default_value())
                            refl->SetString(&msg, field_desc, field_desc->default_value_string());
                       break;


                    default: break;

                }
            }
        }

    };

    auto full_header = pd.header();
    instantiate_defaults(full_header);

    ASSERT_EQ(orig_header_->SerializeAsString(), full_header.SerializeAsString());
}

TEST_P(ProgressiveDecompressorTest, decompress_full)
{
    dpi::ProgressiveDecompressor pd({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                    [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
            },
                "decompress_full_image_"
                    },
        *header_);

    dsl::protobuf::J2KPacketFragmentIdentifier previous_id;
    for(int i = 0, n = fragments_.size(); i < n; ++i)
    {
        const auto& orig_fragment = *orig_fragments_[i];
        const auto& fragment = *fragments_[i];

        ASSERT_EQ(orig_fragment.data().size(), fragment.data().size());
        ASSERT_EQ(orig_fragment.SerializeAsString(), fragment.SerializeAsString());


        ASSERT_TRUE(!previous_id.IsInitialized() || // first packet, first fragment
                    (previous_id.is_last_fragment() &&
                     fragment.id().packet_id() == previous_id.packet_id() + 1 &&
                     fragment.id().fragment_id() == 0) || // this is the start of the next packet
                    (fragment.id().packet_id() == previous_id.packet_id() &&
                     fragment.id().fragment_id() == previous_id.fragment_id() + 1) // this is the next fragment
            );
        previous_id = fragment.id();

        pd.add_fragment(fragment);
    }

    pd.decompress();

    // compare original j2k file to the decompressed one for every byte
    std::ifstream j2k_orig(pc_.j2k_out_path().c_str(), std::ios::in | std::ios::binary);
    std::ifstream j2k_rx(pd.j2k_out_path().c_str(), std::ios::in | std::ios::binary);

    int i = 0;
    while(j2k_orig && j2k_rx)
    {
        ASSERT_EQ(j2k_orig.get(), j2k_rx.get()) << "J2K files differ at byte index " << i;
        ++i;
    }

    ASSERT_TRUE(j2k_orig.eof());
    ASSERT_TRUE(j2k_rx.eof());

    boost::filesystem::remove(pd.j2k_out_path());
}

TEST_P(ProgressiveDecompressorTest, decompress_partial)
{
    dpi::ProgressiveDecompressor pd({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
            },
                "decompress_partial_image_",
                    true // rename partial output files
                },
        *header_);

    dsl::protobuf::J2KPacketFragmentIdentifier previous_id;
    for(int i = 0, n = fragments_.size()/2; i < n; ++i)
    {
        const auto& orig_fragment = *orig_fragments_[i];
        const auto& fragment = *fragments_[i];
        pd.add_fragment(fragment);
    }

    pd.decompress();

    boost::filesystem::remove(pd.j2k_out_path());
}

TEST_P(ProgressiveDecompressorTest, decompress_partial_out_of_order)
{
    dpi::ProgressiveDecompressor pd({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
                    },
                "decompress_partial_image_",
                    true // rename partial output files
                },
        *header_);

    int n = 3*fragments_.size()/4;
    for(int i = 0; i < n; ++i)
    {
        const auto& fragment = *fragments_[i];

        // skip a fragment near the end
        if(i == n-20)
            continue;
        else
            pd.add_fragment(fragment);
    }

    pd.decompress();

    // add the missing fragment
    pd.add_fragment(*fragments_.at(n - 20));
    pd.decompress();

    boost::filesystem::remove(pd.j2k_out_path());
}

TEST_P(ProgressiveDecompressorTest, decompress_for_every_fragment)
{
    dpi::ProgressiveDecompressor pd({
            { image1_out_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
                    },
                "decompress_partial_image_",
                    true // rename partial output files
                },
        *header_);

    for(int i = 0; i < fragments_.size(); ++i)
    {
        const auto& fragment = *fragments_[i];
        //        std::cout << "Adding fragment: " << fragment.ShortDebugString() << std::endl;
        pd.add_fragment(fragment);

        if(fragment.id().is_last_fragment() || fragment.id().packet_id() > 1)
            pd.decompress();

        // ensure compressor and decompressor PSOT calculation are the same
        ASSERT_EQ(pd.available_tile_size(), fragment.cumulative_tile_size());
    }

    boost::filesystem::remove(pd.j2k_out_path());
}


INSTANTIATE_TEST_CASE_P(DifferentImageFiles,
                        ProgressiveDecompressorTest,
                        ::testing::Values(std::make_tuple(0, "test/data/image1_1000w.tif", dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT, false),
                                          std::make_tuple(1, "test/data/image1_1000w.tif", dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT, true),
                                          std::make_tuple(2, "test/data/sentry388_multibeam-image.tif", dpi::ProgressiveCompressorConfiguration::ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT, false),
                                          std::make_tuple(3, "test/data/image1_1000w.tif", dpi::ProgressiveCompressorConfiguration::ProgressionOrder::RESOLUTION_LAYER_COMPONENT_PRECINCT, false)));


//
// Test Managers
//

class ManagerSingleTest : public ::testing::TestWithParam<int> // packet size
{
public:
    ManagerSingleTest() :
        image_dir_("test/data"),
        sender_( {
                { dpi::ImageType::TIFF, image_dir_.native(),
                        [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                        [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
                            //       [](const std::string& s) { std::cout << "Info: " << s << std::endl; }
                },
                    "image([0-9]*)_1000w.tif",
                    { }, // preprocessor defaults
                    { }  // compressor defaults

            })
        {
            dccl_.load_library("libprogressive_imagery.so");
            dccl_.load<dsl::protobuf::J2KMainHeader>();
            dccl_.load<dsl::protobuf::J2KPacketFragment>();
            dccl_.load<dsl::protobuf::J2KQueueUpdateAndAck>();
        }

    virtual void SetUp()
        {
        }

    virtual void TearDown()
        {
        }

    boost::filesystem::path image_dir_;
    dpi::ManagerSender sender_;
    dccl::Codec dccl_;

};


TEST_P(ManagerSingleTest, single_image)
{
    const int requested_size = GetParam();

    std::vector<unsigned char> data;

    sender_.provide_compressed_data(data, requested_size);

    ASSERT_GT(data.size(), 0);
    ASSERT_LE(data.size(), requested_size);

    auto dccl_id = dccl_.id(data.begin(), data.end());
    auto header_dccl_id = dccl_.id<dsl::protobuf::J2KMainHeader>();
    auto fragment_dccl_id = dccl_.id<dsl::protobuf::J2KPacketFragment>();

    // first packet should be header
    ASSERT_EQ(dccl_id, header_dccl_id);

    dsl::protobuf::J2KMainHeader header_out;
    auto next_it = dccl_.decode(data.begin(), data.end(), &header_out);

    ASSERT_EQ(1, header_out.image_id());

    dpi::ProgressiveDecompressor pd({
            { image_dir_.native(),
                    dpi::ImageType::TIFF,
                    [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
                    },
                "decompress_manager_single_",
                    false // rename partial output files
                },
        header_out);

    do
    {

        ASSERT_GT(data.size(), 0);
        ASSERT_LE(data.size(), requested_size);

        while(next_it != data.end())
        {
            auto dccl_id = dccl_.id(next_it, data.end());
            ASSERT_EQ(dccl_id, fragment_dccl_id);
            dsl::protobuf::J2KPacketFragment fragment_out;
            next_it = dccl_.decode(next_it, data.end(), &fragment_out);
            pd.add_fragment(fragment_out);
        }

        sender_.provide_compressed_data(data, requested_size);
        next_it = data.begin();

    }
    while(!data.empty());

    // assert that we received the entire image
    ASSERT_FLOAT_EQ(pd.decompress(), 1);
}

INSTANTIATE_TEST_CASE_P(DifferentPacketSizes,
                        ManagerSingleTest,
                        ::testing::Values(256, 512, 1024, 4096, 6000, 6144));





class ManagerSenderMultipleTest : public ::testing::Test
{
public:
    ManagerSenderMultipleTest() :
        image_dir_("test/data"),
        pass1_frac_(0.1),
        sender_( {
                { dpi::ImageType::TIFF, image_dir_.native(),
                        [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                        [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
                            //                          [](const std::string& s) { std::cout << "Info: " << s << std::endl; }
                },
                    "image([0-9]*)_[0-9]*w.tif",
                    { }, // preprocessor defaults
                    { },  // compressor defaults
                        true, // send existing images
                            "preprocess", // preprocess folder
                            pass1_frac_ // pass1 frac

            })
        {
            dccl_.load_library("libprogressive_imagery.so");
            dccl_.load<dsl::protobuf::J2KMainHeader>();
            dccl_.load<dsl::protobuf::J2KPacketFragment>();
            dccl_.load<dsl::protobuf::J2KQueueUpdateAndAck>();
        }

    virtual void SetUp()
        {
        }

    virtual void TearDown()
        {
        }

    boost::filesystem::path image_dir_;
    float pass1_frac_;
    dpi::ManagerSender sender_;
    dccl::Codec dccl_;

};

TEST_F(ManagerSenderMultipleTest, actions_list)
{
    ASSERT_EQ(sender_.level2_actions().size(), 4);
    auto it = sender_.level2_actions().begin();

    // pass 1
    ASSERT_EQ(it->image_it_->compressor().image_id(), 1);
    ASSERT_FLOAT_EQ(it->action_.fraction_goal, pass1_frac_);
    ++it;
    ASSERT_EQ(it->image_it_->compressor().image_id(), 2);
    ASSERT_FLOAT_EQ(it->action_.fraction_goal, pass1_frac_);
    ++it;

    // pass 2
    ASSERT_EQ(it->image_it_->compressor().image_id(), 1);
    ASSERT_FLOAT_EQ(it->action_.fraction_goal, 1.0);
    ++it;
    ASSERT_EQ(it->image_it_->compressor().image_id(), 2);
    ASSERT_FLOAT_EQ(it->action_.fraction_goal, 1.0);
    ++it;

}


TEST_F(ManagerSenderMultipleTest, two_images)
{
    const int requested_size = 256;
    const auto header_dccl_id = dccl_.id<dsl::protobuf::J2KMainHeader>();
    const auto fragment_dccl_id = dccl_.id<dsl::protobuf::J2KPacketFragment>();

    std::vector<unsigned char> data;
    sender_.provide_compressed_data(data, requested_size);

    std::map<int, dpi::ProgressiveDecompressor> pds;

    auto next_it = data.begin();

    int toggle_count = 0;
    int previous_image_id = 0;

    do
    {

        ASSERT_GT(data.size(), 0);
        ASSERT_LE(data.size(), requested_size);

        while(next_it != data.end())
        {
            auto dccl_id = dccl_.id(next_it, data.end());
            if(dccl_id == header_dccl_id)
            {
                dsl::protobuf::J2KMainHeader header_out;
                next_it = dccl_.decode(next_it, data.end(), &header_out);

                // correct order
                if(pds.empty())
                    ASSERT_EQ(1, header_out.image_id());
                else
                    ASSERT_EQ(2, header_out.image_id());

                pds.emplace(header_out.image_id(),
                            dpi::ProgressiveDecompressor( {
                                    { image_dir_.native(),
                                            dpi::ImageType::TIFF,
                                            [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
                                    },
                                        "decompress_manager_two_images_",
                                            false // rename partial output files
                                            },
                                header_out)
                    );

            }
            else if(dccl_id == fragment_dccl_id)
            {
                dsl::protobuf::J2KPacketFragment fragment_out;
                next_it = dccl_.decode(next_it, data.end(), &fragment_out);
                pds.at(fragment_out.id().image_id()).add_fragment(fragment_out);

                if(previous_image_id != 0 &&
                   previous_image_id != fragment_out.id().image_id())
                {
                    ++toggle_count;
                    switch(toggle_count)
                    {
                        case 1:
                        {
                            auto frac = pds.at(1).decompress();
                            ASSERT_GE(frac, pass1_frac_);
                            ASSERT_LT(frac, 1.0);
                            break;
                        }

                        case 2:
                        {
                            auto frac = pds.at(2).decompress();
                            ASSERT_GE(frac, pass1_frac_);
                            ASSERT_LT(frac, 1.0);
                            break;
                        }

                        case 3:
                            ASSERT_FLOAT_EQ(pds.at(1).decompress(), 1.0);
                            break;
                    }
                }
                previous_image_id = fragment_out.id().image_id();
            }
        }

        sender_.provide_compressed_data(data, requested_size);
        next_it = data.begin();
    }
    while(!data.empty());

    // assert that we received the entire image
    ASSERT_FLOAT_EQ(pds.at(2).decompress(), 1);
}



class ManagersMultipleTest : public ::testing::Test
{
public:
    ManagersMultipleTest() :
        image_dir_("test/data"),
        pass1_frac_(0.2),
        sender_( {
                { dpi::ImageType::TIFF, image_dir_.native(),
                        [](const std::string& s) { std::cout << "Error: " << s << std::endl; },
                        [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
                },
                    "image([0-9]*)_[0-9]*w.tif",
                    { }, // preprocessor defaults
                    { },  // compressor defaults
                        true, // send existing images
                            "preprocess", // preprocess folder
                            pass1_frac_ // pass1 frac

                            }),
        receiver_( {
                { dpi::ImageType::TIFF, image_dir_.native(),
                        [](const std::string& s) { std::cout << "Error: " << s << std::endl; }
                        // [](const std::string& s) { std::cout << "Warning: " << s << std::endl; }
                        //[](const std::string& s) { std::cout << "Info: " << s << std::endl; }
                },
                { { }, "managers_rx_",
                           false // rename partial
                  } // decompressor defaults
            })

        {
            dccl_.load_library("libprogressive_imagery.so");
            dccl_.load<dsl::protobuf::J2KQueueUpdateAndAck>();
        }

    virtual void SetUp()
        {
        }

    virtual void TearDown()
        {
        }

    boost::filesystem::path image_dir_;
    float pass1_frac_;
    dpi::ManagerSender sender_;
    dpi::ManagerReceiver receiver_;
    dccl::Codec dccl_;

};

TEST_F(ManagersMultipleTest, two_images)
{
    const int requested_size = 1024;

    auto ack_dccl_id = dccl_.id<dsl::protobuf::J2KQueueUpdateAndAck>();

    int i = 0;
    std::vector<unsigned char> data;
    do
    {
        ASSERT_EQ(sender_.level0_queue().size(), 0);
        ASSERT_EQ(sender_.level1_queue().size(), 0);
        sender_.clear_staging_queue();
        sender_.provide_compressed_data(data, requested_size);

        auto result1 = receiver_.post_compressed_data(data);
        // intentionally dup all packets to test correct behavior
        auto result2 = receiver_.post_compressed_data(data);

        ASSERT_EQ(result1.size(), result2.size());

        //        std::cout << "request " << i << ": " <<std::endl;
        auto it2 = result2.begin();
        for(const auto& id : result1)
        {
            //            std::cout << "Updated: " << id << std::endl;
            ASSERT_EQ(id, *it2++);
        }

        //        std::cout << "ack_data.size(): " << std::get<1>(result1).size() << std::endl;

        // request ack data
        std::vector<std::vector<unsigned char>> acks;
        do
        {
            acks.emplace_back();
            receiver_.provide_compressed_data(acks.back(), requested_size);
        }
        while(!acks.back().empty());

        acks.pop_back(); // empty

        for(const auto& ack_data : acks)
        {
            ASSERT_LE(ack_data.size(), requested_size);

            auto next_it = ack_data.begin();
            // std::cout << "encoded size: " << ack_data.size() << std::endl;
            while(next_it != ack_data.end())
            {
                auto dccl_id = dccl_.id(next_it, ack_data.end());
                ASSERT_EQ(dccl_id, ack_dccl_id);
                dsl::protobuf::J2KQueueUpdateAndAck ack;
                next_it = dccl_.decode(next_it, ack_data.end(), &ack);

                ASSERT_LE(ack.ack_size(), dsl::protobuf::J2KQueueUpdateAndAck::descriptor()->FindFieldByName("ack")->options().GetExtension(dccl::field).max_repeat());

                //std::cout << "Ack: " << ack.ShortDebugString() << std::endl;
            }

            sender_.post_compressed_data(ack_data);
        }


        ++i;
    } while (data.size());
}



int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
