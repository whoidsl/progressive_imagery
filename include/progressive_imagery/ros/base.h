/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef PROGRESSIVE_IMAGERY_BASE_20180615H
#define PROGRESSIVE_IMAGERY_BASE_20180615H

// common
#include "ros/ros.h"
#include "progressive_imagery/manager.h"
#include "ds_acomms_msgs/ModemData.h"
#include "ds_acomms_msgs/ModemDataRequest.h"
#include "ds_acomms_msgs/AcksExpected.h"
#include "progressive_imagery/QueueAction.h"
#include <boost/functional/hash.hpp>
#include <iostream>

namespace dsl
{
    namespace progressive_imagery
    {
        namespace ros
        {
            class NodeBase
            {
            public:
                NodeBase();

                void run(int argc, char** argv);

            protected:
                virtual void loop() = 0;
                virtual void data_received_event(const std::set<int>& updated_ids) {}
                virtual void data_request_event() {}
                virtual ManagerBase& manager() = 0;

                ::ros::NodeHandle& node_handle()
                { return node_handle_; }

            private:
                void handle_incoming_data(const ::ds_acomms_msgs::ModemData& data);
                bool handle_data_request(::ds_acomms_msgs::ModemDataRequest::Request& req,
					 ::ds_acomms_msgs::ModemDataRequest::Response& res);
		void handle_acks_expected(const ::ds_acomms_msgs::AcksExpected& msg);

                void handle_queue_update(const ::progressive_imagery::QueueAction& action);

            private:
                ::ros::NodeHandle node_handle_;
		::ros::ServiceServer data_server_;
                ::ros::Subscriber rx_data_sub_;
		::ros::Subscriber acks_sub_;
                ::ros::Subscriber queue_update_sub_;

            };
        }
    }
}


#endif
