/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef MANAGER_20180329H
#define MANAGER_20180329H

#include <sys/inotify.h>

#include <regex>
#include <dccl.h>

#include "progressive_imagery/common.h"
#include "progressive_imagery/progressive_codec.h"
#include "progressive_imagery/preprocess.h"

namespace dsl
{
    namespace progressive_imagery
    {
        /// For convenience, the header DCCL packet is referred to as -1 (where the first tile packet is 0).
        constexpr int HEADER_PACKET_ID {-1};

        /// \brief Configuration values that are used for both the ManagerSender and ManagerReceiver
        struct ManagerCommonConfiguration
        {
            /// The type of image to be read (ManagerSender) or written (ManagerReceiver)
            ImageType type { ImageType::TIFF };
            /// String to a directory to read from (if role == SENDER) or write to (if role == RECEIVER) the image files.
            std::string img_dir;

            /// Function callback for "error" messages (most critical). If not set, messages are not posted. Will also be used for ProgressiveCompressor and ProgressiveDecompressor messages.
            std::function<void(const std::string&)> error_callback;
            /// Function callback for "warning" messages (important but likely not critical). If not set, messages are not posted. Will also be used for ProgressiveCompressor and ProgressiveDecompressor messages.
            std::function<void(const std::string&)> warning_callback;
            /// Function callback for "info" messages (debug information). If not set, messages are not posted. Will also be used for ProgressiveCompressor and ProgressiveDecompressor messages.
            std::function<void(const std::string&)> info_callback;

        };

        ///\brief Struct for holding configuration values for the ManagerSender
        struct ManagerSenderConfiguration
        {
            /// The configuration values that are used by both the ManagerSender and ManagerReceiver
            ManagerCommonConfiguration common;

            /// Regex for new images; must capture one marked subexpression i.e. "()" which is a sequential image id
            std::string image_filename_regex { "^image_([0-9]*)\\.[a-zA-Z]{3,4}$" };

            /// Set the default configuration fo the PreProcessor
            PreProcessorConfiguration preprocessor_defaults;

            /// Set the default configuration for the ProgressiveCompressor
            ProgressiveCompressorConfiguration compressor_defaults;

            /// If true, send all the existing images in the directory, and any new ones. If false, only send images that are written after starting this ManagerSender
            bool send_existing_images { true };

            /// Subfolder of common.img_dir for writing intermediate images (the output of the PreProcessor)
            std::string preprocess_subfolder { "preprocess" };

            /// The fraction of image to send in pass 1. Once this fraction of all the images has been sent, the remainder of each image is sent.
            float pass1_fraction { 0.5 };

            /// If set, called when a new image is encoded, with the J2K header information
            std::function<void(const dsl::protobuf::J2KMainHeader& header, int dccl_encoded_bytes)> encode_callback;
        };

        /// \brief Configuration for the ManagerReceiver
        struct ManagerReceiverConfiguration
        {
            /// Common configuration values
            ManagerCommonConfiguration common;

            /// Set the default configuration for the decompressor
            ProgressiveDecompressorConfiguration decompressor_defaults;
        };


        /// \brief Represents a Queue goal action
        struct QueueAction
        {
            int image_id;
            float fraction_goal;
        };

        ///\brief Base class for the ManagerSender and ManagerReceivers, which are the highest level interface into the progressive imagery library
        class ManagerBase
        {
        public:

            /// \brief Construct
            /// \param cfg Configuration object
            /// \throw Exception Directory doesn't exist or isn't readable/writable as needed
            ManagerBase(ManagerCommonConfiguration cfg);

            virtual ~ManagerBase() { }

            /// \brief The role (SENDER or RECEIVER) of this Manager
            virtual dsl::progressive_imagery::Role role() = 0;

            /// \brief Check for new images and if so, queue the.
            /// \return Number of new images queued
            virtual int check_for_new_images() { return 0; }


            /// \brief Post received compressed (DCCL) data
            /// \param data Compressed data containing one or more DCCL messages
            /// \return set of updated image ids
            virtual std::set<int> post_compressed_data(const std::vector<unsigned char>& data) = 0;

            /// \brief Provide compressed (using DCCL) data
            /// \param data Reference to vector for the output data. This will be resized to the actual returned data size. This may contain one or more DCCL messages (concatenated). If no data are available, data will be empty after this call
            /// \param max_bytes the maximum size of the returned data (after calling, data.size() <= max_bytes)
            virtual std::vector<std::shared_ptr<google::protobuf::Message>> provide_compressed_data(std::vector<unsigned char>& data, int max_bytes) = 0;

            /// \brief Clear any messages that have been provided, but not acknowledged. Typically done after a chance from acknowledgments has occured during the TDMA cycle.
            virtual void clear_staging_queue() {}

            /// \brief Update the sender queue, or on the receiver side, queue these updates to be transmitted to the sender
            virtual void post_queue_update(const std::vector<QueueAction>& actions) = 0;


        protected:
            template<typename CodecConfig>
                void set_codec_defaults(CodecConfig& codec_cfg)
            {
                codec_cfg.common.info_callback = cfg_.info_callback;
                codec_cfg.common.warning_callback = cfg_.warning_callback;
                codec_cfg.common.error_callback = cfg_.error_callback;
                codec_cfg.common.out_dir = cfg_.img_dir;
            }

            bool encode(std::vector<unsigned char>& data, int max_bytes, std::shared_ptr<google::protobuf::Message> msg)
            {
                auto previous_data_size = data.size();
                auto next_msg_size = dccl().size(*msg);
                if(previous_data_size + next_msg_size <= max_bytes)
                {
                    data.resize(previous_data_size + next_msg_size);
                    dccl().encode(reinterpret_cast<char*>(data.data())+previous_data_size, next_msg_size, *msg);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            dccl::Codec& dccl() { return dccl_; }

        private:
            ManagerCommonConfiguration cfg_;
            dccl::Codec dccl_;
        };

        /// \brief The main high level interface to the progressive imagery library for sending of images
        ///\ingroup api_0
        class ManagerSender : public ManagerBase
        {
        private:
            ManagerSenderConfiguration cfg_;
            std::regex filename_regex_;

            //
            // Staging Queue (Level 0) - messages that have been sent between clearing the staging area (to be done after acks have been received)
            //
            std::map<dsl::protobuf::J2KPacketFragmentIdentifier, std::shared_ptr<google::protobuf::Message>> level0_queue_;

            //
            // Level 1 Queue - messages that have been sent, but are not yet acknowledged
            //
            //
            std::map<dsl::protobuf::J2KPacketFragmentIdentifier, std::shared_ptr<google::protobuf::Message>> level1_queue_;


            //
            // Level 2 Queue - the images themselves
            //
            class ImageTxMeta
            {
            private:
                ProgressiveCompressor compressor_;

                std::set<dsl::protobuf::J2KPacketFragmentIdentifier> fragments_sent_;
                std::set<dsl::protobuf::J2KPacketFragmentIdentifier> fragments_acked_;
                int number_fragments_plus_header_ {0};


                // only keep these in memory if actively sending the image
                std::shared_ptr<dsl::protobuf::J2KMainHeader> header_;
                std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>> fragments_;
                typename decltype(fragments_)::const_iterator next_fragment_;

            private:
                bool header_sent()
                { return !fragments_sent_.empty(); }


            public:
                ImageTxMeta(ProgressiveCompressor c) : compressor_(std::move(c)) { }

                ProgressiveCompressor& compressor() { return compressor_; }
                const ProgressiveCompressor& compressor() const { return compressor_; }


                bool all_fragments_sent() const
                { return !fragments_sent_.empty() && fragments_sent_.size() == number_fragments_plus_header_; }

                bool no_fragments_sent() const
                { return fragments_sent_.empty(); }

                bool image_data_is_allocated() const
                { return header_ && !fragments_.empty(); }

                void allocate_image_data();
                void deallocate_image_data();

                float sent_fraction() const
                {
                    if(fragments_sent_.size() <= 1)
                        return 0;
                    else
                        return (static_cast<float>((*(next_fragment_-1))->cumulative_tile_size()))/
                            header_->sot().psot();
                }

                // warning: may be inaccurate if acks have gaps
                float received_fraction()
                {
                    // don't count header
                    if(fragments_acked_.size() <= 1)
                    {
                        return 0;
                    }
                    else
                    {
                        // no guarantee this image is still allocated
                        bool image_data_allocated = image_data_is_allocated();
                        if(!image_data_allocated)
                            allocate_image_data();

                        using namespace std::rel_ops;
                        auto last_fragment = next_fragment_;
                        --last_fragment; // this fragment hasn't been sent yet
                        const auto& last_fragment_acked = *fragments_acked_.rbegin();
                        while(last_fragment != fragments_.begin() && (*last_fragment)->id() > last_fragment_acked)
                            --last_fragment;

                        auto v = (static_cast<float>((*(last_fragment))->cumulative_tile_size()))/
                            header_->sot().psot();

                        if(!image_data_allocated)
                            deallocate_image_data();
                        return v;
                    }
                }

                // look at the next message or return an empty pointer if no more messages
                std::shared_ptr<google::protobuf::Message> peek_next_message();

                // retrieve the next message and mark it as sent, and increment next_fragment
                std::pair<dsl::protobuf::J2KPacketFragmentIdentifier, std::shared_ptr<google::protobuf::Message>> get_next_message();

                void post_ack_message(const dsl::protobuf::J2KPacketFragmentIdentifier& ack)
                { fragments_acked_.insert(ack); }


            };

            // list of images (in the order they are added to the directory)
            std::list<ImageTxMeta> level2_image_queue_;

            // map of image id to tx_images iterator (since level2_image_queue_ are not necessarily in order by image id)
            std::map<int, decltype(level2_image_queue_)::iterator> image_id_to_level2_queue_it_;

            // queue of requested actions, and default actions
            // {front} [ requests ] [ pass1 defaults ] [ pass 2 defaults ] {back}
            // new requests get put on the front. New images get put on the end of pass 1 and pass 2 defaults
            struct ImageTxAction
            {
                decltype(level2_image_queue_)::iterator image_it_;
                QueueAction action_;
                bool user_action_ { false }; // is a user specified action (as opposed to default)
            };

            std::list<ImageTxAction> level2_actions_;

            // for reading inotify to catch new files in the image directory
            std::array<char, 10*sizeof(struct inotify_event) + NAME_MAX +1> inotify_buffer_;
            int inotify_fd_ { inotify_init1(IN_NONBLOCK) };
            int inotify_wd_ { 0 };

        private:
            dsl::progressive_imagery::Role role() override
            { return dsl::progressive_imagery::Role::SENDER; }

            // true if the image is queued, false otherwise
            bool queue_image(const boost::filesystem::path& image_path);

        public:
            /// Construct with the given configuration
            ManagerSender(ManagerSenderConfiguration cfg);
            /// Check for new images in the configured directory (cfg.img_dir), and if they match the configured regex (cfg.image_filename_regex), queue them
            int check_for_new_images() override;

            /// \brief Provide compressed (using DCCL) header and/or fragments as determined by the queue actions.
            /// \param data Reference to a byte vector for storing the encoded data
            /// \param max_bytes maximum number of bytes to return
            /// \return Unencoded DCCL/Protobuf messages that were used to create the encoded data
            std::vector<std::shared_ptr<google::protobuf::Message>> provide_compressed_data(std::vector<unsigned char>& data, int max_bytes) override;

            /// \brief Post DCCL compressed J2KQueueUpdateAndAck message(s)
            /// \return set of updated image ids
            std::set<int> post_compressed_data(const std::vector<unsigned char>& acks) override;

            std::set<int> post_queue_update_and_ack(const dsl::protobuf::J2KQueueUpdateAndAck& queue_update_and_ack);

            /// \brief Clears the staging area so that unacknowledged messages will be resent
            void clear_staging_queue() override
            { level0_queue_.clear(); }

            /// \brief Posts user updates to the action queue
            void post_queue_update(const std::vector<QueueAction>& actions) override;


            /// Returns the level0 queue (staging between ACK events)
            const decltype(level0_queue_)& level0_queue() const
            { return level0_queue_; }

            /// Returns the level1 queue (unacked messages)
            const decltype(level1_queue_)& level1_queue() const
            { return level1_queue_; }

            /// Returns a reference to the level2 actions queue, for inspection of the order of images (and fractions of images) to be sent
            const decltype(level2_actions_)& level2_actions() const
            { return level2_actions_; }

            /// Returns the level2 image queue itself
            const decltype(level2_image_queue_)& level2_image_queue() const
            { return level2_image_queue_; }

            /// Returns the iterator to the level
            ImageTxMeta& level2_image_meta(int image_id)
            {
                auto level2_queue_it = image_id_to_level2_queue_it_.find(image_id);
                if(level2_queue_it != image_id_to_level2_queue_it_.end())
                    return *(level2_queue_it->second);
                else
                    throw(dsl::progressive_imagery::Exception("No image id " + std::to_string(image_id) + " loaded"));
            }
        };

        /// \brief The main high level interface to the progressive imagery library for receiving images
        ///\ingroup api_0
        class ManagerReceiver : public ManagerBase
        {

        private:
            ManagerReceiverConfiguration cfg_;

            struct ImageRxMeta
            {
                ProgressiveDecompressor decompressor;
                float fraction_available { 0 };
            };

            std::map<int, ImageRxMeta> rx_images_;
            std::set<dsl::protobuf::J2KPacketFragmentIdentifier> pending_acks_;

            // if we resend this ack, resend the pending_queue_actions_. This should help ensure that the queue updates get through, even if the ack message is lost
            dsl::protobuf::J2KPacketFragmentIdentifier latest_ack_with_queue_action_;
            std::vector<QueueAction> pending_queue_actions_;


        private:
            dsl::progressive_imagery::Role role() override
            { return dsl::progressive_imagery::Role::RECEIVER; }

        public:
            /// Construct with the given configuration
            ManagerReceiver(ManagerReceiverConfiguration cfg) : ManagerBase(cfg.common),
                cfg_(std::move(cfg))

                {
                    set_codec_defaults(cfg_.decompressor_defaults);
                }

            /// \brief Post received compressed (DCCL) data
            /// \param data Compressed data containing one or more DCCL J2KMainHeader or J2KPacketFragment messages
            /// \return set of updated image ids
            std::set<int> post_compressed_data(const std::vector<unsigned char>& data) override;

            void post_header(const dsl::protobuf::J2KMainHeader& header);
            std::set<int> post_fragment(const dsl::protobuf::J2KPacketFragment fragment);

            /// \brief Provide compressed (using DCCL) ack messages
            /// \param data Reference to vector for the output data. This will be resized to the actual returned data size. This may contain one or more DCCL messages (concatenated). If no data are available, data will be empty after this call
            /// \param max_bytes the maximum size of the returned data (after calling, data.size() <= max_bytes)
            std::vector<std::shared_ptr<google::protobuf::Message>> provide_compressed_data(std::vector<unsigned char>& data, int max_bytes) override;

            /// \brief Buffers queue updates to be sent to the sender with the next batch of acks
            void post_queue_update(const std::vector<QueueAction>& actions) override;


            boost::filesystem::path id_to_path(int image_id)
            {
                auto it = rx_images_.find(image_id);
                if(it != rx_images_.end())
                    return it->second.decompressor.image_out_path();
                else
                    return boost::filesystem::path();
            }

            const decltype(rx_images_)& rx_images() const
            { return rx_images_; }

        };


    }
}

#endif
