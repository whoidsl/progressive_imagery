/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef PREPROCESS_20180329H
#define PREPROCESS_20180329H

#include <Magick++.h>

#include "progressive_imagery/common.h"

namespace dsl
{
    namespace progressive_imagery
    {
        /// Configuration for the PreProcessor
        struct PreProcessorConfiguration
        {
            /// Full path to the input image file (to read)
            std::string in_file;
            /// Full path to the output image file (to write)
            std::string out_file;
            /// Pixel maximum width for the output image. The image will only be reduced, not enlarged.
            int max_width { 1920 };
            /// Pixel maximum height for the output image. The image will only be reduced, not enlarged.
            int max_height { 1080 };
            /// If true, convert the image to grayscale
            bool make_grayscale { false };
        };

        /// \brief Uses Magick++ to potentially resize and grayscale the source image before passing it to the ProgressiveCompressor. 
        /// \details By resizing the image (and possibly grayscaling), we greatly reduce the CPU overhead of JPEG2000-compressing large images that will never be sent at full resolution, and the RAM overhead of queuing them. Images will not be enlarged if smaller than the maximum dimensions.
        ///\ingroup api_1
        class PreProcessor
        {
        public:
            /// \brief Configure the PreProcessor
            PreProcessor(PreProcessorConfiguration cfg, bool auto_process = false);
            ~PreProcessor();
            
            /// \brief Perform the conversion
            void process();
            /// \brief Write the output file
            void write();            

            /// \brief Reference to the ImageMagick image object
            /// \details Processing is done in place on this image object. Thus, before process(), this is the input image. After process(), this is the output image.
            const Magick::Image& image() { return image_; }
            
        private:
            PreProcessorConfiguration cfg_;
            Magick::Image image_;
        };
    }
}












#endif
