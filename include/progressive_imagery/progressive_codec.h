/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef PROGRESSIVE_CODEC_20180330H
#define PROGRESSIVE_CODEC_20180330H

#include <memory>
#include <tuple>
#include <fstream>
#include <iostream>
#include <utility>

#include <boost/filesystem.hpp>
#include <openjpeg.h>

#include "progressive_imagery/common.h"
#include "progressive_imagery/exception.h"

#include "progressive_imagery/jpeg2000-image.pb.h"

/// Deep Submergence Lab
namespace dsl
{
    /// Progressive Image transmission 
    namespace progressive_imagery
    {
        /// Configuration used by both the ProgressiveCompressor and ProgressiveDecompressor
        struct ProgressiveCodecCommonConfiguration
        {
            /// Full path to the output directory for writing the J2K file and output images (for Decompressor)
            std::string out_dir;

            /// The expected input image type (for compressor) or requested output image type (for decompressor)
            ImageType type { ImageType::TIFF };

            /// Function callback for "error" level messages (most critical)
            std::function<void(const std::string&)> error_callback;
            /// Function callback for "warning" level messages (important but likely not critical)
            std::function<void(const std::string&)> warning_callback;
            /// Function callback for "info" level messages (debug information)
            std::function<void(const std::string&)> info_callback;
        };

        /// Configuration for the ProgressiveCompressor
        struct ProgressiveCompressorConfiguration
        {
            /// Configuration shared with both the Compressor and Decompressor
            ProgressiveCodecCommonConfiguration common;
            /// Image identification number
            int image_id;
            /// Full path to the input image file (to read)
            std::string in_file;

            /// The ordering of packets in JPEG2000 (listed from outermost loop to innermost loop)
            enum class ProgressionOrder
            {
                /// Quality-layer progression
                LAYER_RESOLUTION_COMPONENT_PRECINCT,
                /// Resolution progression
                RESOLUTION_LAYER_COMPONENT_PRECINCT
            };
            /// JPEG2000 progression ordering for packets
            ProgressionOrder progression_order { ProgressionOrder::LAYER_RESOLUTION_COMPONENT_PRECINCT };
            /// Number of resolutions in the JPEG2000 codestream
            int number_of_resolutions { 6 };
            /// Number of quality layers in the JPEG2000 codestream
            int number_of_layers { 10 };

            /// Approximate maximum size of the JPEG2000 codestream
            int max_approx_bytes { 20480 };
            /// Approximate minimum size to render some of the JPEG2000 image
            int min_approx_bytes { 640 };
        };
        
        /// Configuration for the ProgressiveDecompressor
        struct ProgressiveDecompressorConfiguration
        {
            /// Configuration shared with both the Compressor and Decompressor
            ProgressiveCodecCommonConfiguration common;

            /// Prefix for output files, to be followed with the image ID and extension, e.g. "image_00014.tif"
            std::string filename_prefix { "image_" };

            /// Rename partial output files for later storage (versus overwriting). If true, rename to percentage and bytes of the J2K tile available, e.g. "image_00014_04pct_0001240B.tif"
            bool rename_partial { false };
        };

        /// \brief Helper class for JPEG2000 codestream markers
        struct J2KMarker
        {
            /// Marker size (in bytes)
            static constexpr int MARKER_SIZE = 2;
            /// Codestream markers using shorthand from JPEG2000 standard
            enum J2KMarkerEnum
            {
                /// Start Of Codestream
                SOC = 0xFF4F,
                /// image and tile SIZe
                SIZ = 0xFF51,
                /// Quantization Default
                QCD = 0xFF5C,
                /// Start Of Tile
                SOT = 0xFF90,
                /// COding style Default
                COD = 0xFF52,
                /// COMment
                COM = 0xFF64,
                /// Start Of Packet
                SOP = 0xFF91,
                /// End Of Codestream
                EOC = 0xFFD9
            };
        };

        /// \brief RAII tool that provides exception safety by removing the image file if the image operation (compress/decompress) fails (throws an exception) before it succeeds.
        /// \details This avoids partial image files being left on disk in the event of an exception. Used by ProgressiveCompressor and ProgressiveDecompressor
        struct ImageRemover
        {
            /// Construct with the path to the image to be removed if the destructor is called before commit() is called
            ImageRemover(const boost::filesystem::path& img) : img_(img) { }
            /// If the image is not committed (using commit()), remove the image
            ~ImageRemover() { if(!committed_) boost::filesystem::remove(img_); }
            /// Once the image operation(s) succeed, call this to ensure ImageRemover does not remove the image
            void commit() { committed_ = true; }
        private:
            const boost::filesystem::path& img_;
            bool committed_ {false};
        };

        // returns ceil(numerator/denominator) for integers
        template<typename Integer>
            inline Integer intceil(Integer numerator, Integer denominator)
        { return (numerator + denominator - 1) / denominator; }

        // from SOT to first SOP
        inline int calculate_header_tile_size(const dsl::protobuf::J2KMainHeader& header)
        {
            // start with the sot marker segment 
            int tile_size = header.sot().length() + J2KMarker::MARKER_SIZE;
            // SOD marker
            tile_size += J2KMarker::MARKER_SIZE;
            return tile_size;    
        }

        // from SOP to start of packet header
        inline int fragment_header_size()
        {
            int tile_size = J2KMarker::MARKER_SIZE;
            constexpr int length_size = 2;
            tile_size += length_size;
            const int nsop_size = 2;
            tile_size += nsop_size;
            return tile_size;
        }
        
        
        /// \brief Use OpenJPEG to compress the input image into JPEG2000 and packetize. One instantiation of this class per image
        ///\ingroup api_1
        class ProgressiveCompressor
        {
        public:
            /// \brief Configure the ProgressiveCompressor
            ProgressiveCompressor(ProgressiveCompressorConfiguration cfg);
            /// Default move constructor
            ProgressiveCompressor(ProgressiveCompressor&& c) = default;
            ~ProgressiveCompressor();

            /// \brief Compress the image
            void compress();

            /// \brief Packetize the image using DCCL messages
            std::tuple<std::shared_ptr<dsl::protobuf::J2KMainHeader>, std::deque<std::shared_ptr<dsl::protobuf::J2KPacketFragment>>> packetize();
            
            /// \brief Access the OpenJPEG image
            const opj_image_t& image() const
            {
                if(image_) return *image_;
                else throw(Exception("Null image accessed using image()."));
            }

            /// \brief Access the OpenJPEG compression parameters
            const opj_cparameters_t& parameters() const
            { return parameters_; }

            /// \brief Access the directory where the J2K (JPEG2000 codestream) files are written
            const boost::filesystem::path& j2k_out_path() const
            { return j2k_out_path_; }

            /// \brief Accesss the image ID of this ProgressiveCompressor
            int image_id() const
            { return cfg_.image_id; }            

        private:
            void read_j2k_marker_segment(std::ifstream& j2k_ifs, google::protobuf::Message* seg);            
            
            template<typename Integer, typename Container>
                Integer to_big_endian(Container c)
            {
                Integer r = 0;
                for(int i = 0, n = c.size(); i < n; ++i)
                    r += (static_cast<Integer>(c[i]) & 0xFF) << 8*(n-i-1);
                return r;
            }

            void throw_non_matching_default_value(const google::protobuf::FieldDescriptor* field_desc, std::string expected, std::string received)
            {
                throw(Exception(std::string("Value does not match default on field \"" + field_desc->full_name() + "\" tagged (dccl.field).omit = true. Expected: ") + expected + ", received: " + received));
            }
            
            template<typename T>
                void throw_non_matching_default_value(const google::protobuf::FieldDescriptor* field_desc, T expected, T received)
            {
                throw_non_matching_default_value(field_desc, std::to_string(expected), std::to_string(received));
            }
            
            
        private:            
            ProgressiveCompressorConfiguration cfg_;
            boost::filesystem::path image_in_path_;
            boost::filesystem::path j2k_out_path_;
            opj_cparameters_t parameters_;
            std::unique_ptr<opj_image_t, decltype(&opj_image_destroy)> image_;
        };


        /// \brief Use OpenJPEG to decompress the output image into the requested output type. One instantiation of this class per decompressed image
        ///\ingroup api_1
        class ProgressiveDecompressor
        {
        public:
            /// \brief Configure the ProgressiveCompressor
            ProgressiveDecompressor(ProgressiveDecompressorConfiguration cfg,
                                    dsl::protobuf::J2KMainHeader header);
            /// Default move constructor
            ProgressiveDecompressor(ProgressiveDecompressor&& d) = default;
            
            ~ProgressiveDecompressor();

            void add_fragment(const dsl::protobuf::J2KPacketFragment& fragment)
            {
                if(fragment.id().image_id() != header_.image_id())
                    throw(Exception("Fragment image id does not match header image id"));

                auto result = fragments_.insert(fragment);
                if(!result.second && cfg_.common.warning_callback)
                    cfg_.common.warning_callback("Failed to add fragment: duplicate?");
                
            }
            
            
            /// \brief Decompress the image using the existing data
            ///
            /// \return fraction of image decompressed by bytes (0-1, where 1 is the full image)
            float decompress();
            
            /// \brief Access the OpenJPEG image
            const opj_image_t& image() const
            {
                if(image_) return *image_;
                else throw(Exception("Null image accessed using image()."));
            }

            /// \brief Access the OpenJPEG compression parameters
            const opj_dparameters_t& parameters() const
            { return parameters_; }

            /// \brief Access the path to the final decompressed and decoded (e.g. TIF) file
            const boost::filesystem::path& image_out_path() const
            { return image_out_path_; }

            /// \brief Access the path to the J2K (JPEG2000 codestream) file 
            const boost::filesystem::path& j2k_out_path() const
            { return j2k_out_path_; }
            
            /// \brief Access the (DCCL version of the) J2K header
            const dsl::protobuf::J2KMainHeader& header() const
            { return header_; }

            /// \brief Accesss the image ID
            int image_id() const
            { return header_.image_id(); }            

            
            /// \brief May be called after calling decompress(); provides the number of available JPEG2000 tile bytes in the fragments received thus far.
            int available_tile_size() const
            { return available_psot_size_; };
            
        private:
            void populate_header();

            void write_j2k_marker_segment(std::ofstream& j2k_ofs, const google::protobuf::Message& seg);

            template<typename Container, typename Integer>
                Container from_big_endian(Integer value, int num_bytes)
            {
                Container c;
                c.resize(num_bytes);
                for(int i = 0; i < num_bytes; ++i)
                    c[i] = (value >> 8*(num_bytes-i-1)) & 0xFF;
                return c;
            }

            void reassemble_j2k();
            void decode();

            
        private:
            ProgressiveDecompressorConfiguration cfg_;
            dsl::protobuf::J2KMainHeader header_;
            std::set<dsl::protobuf::J2KPacketFragment> fragments_;
            boost::filesystem::path image_out_path_;
            boost::filesystem::path j2k_out_path_;
            opj_dparameters_t parameters_;
            std::unique_ptr<opj_image_t, decltype(&opj_image_destroy)> image_;

            // current tile size given available fragments
            int available_psot_size_;
            std::streampos psot_pos_ { 0 };
        };
    }
}

namespace dsl
{
    /// Contains the Google Protocol Buffers / DCCL messages (reading the .proto files directly may be more useful than the Doxygen output from the autogenerated C++ code)
    namespace protobuf
    {
        inline bool operator<(const J2KPacketFragmentIdentifier &f1, const J2KPacketFragmentIdentifier &f2)
        {   
            if(f1.image_id() != f2.image_id())
                return f1.image_id() < f2.image_id();
            else if(f1.packet_id() != f2.packet_id())
                return f1.packet_id() < f2.packet_id();
            else
                return f1.fragment_id() < f2.fragment_id();
        }

        inline bool operator==(const J2KPacketFragmentIdentifier &f1, const J2KPacketFragmentIdentifier &f2)
        {
            return f1.image_id() == f2.image_id() && f1.packet_id() == f2.packet_id() && f1.fragment_id() == f2.fragment_id();
        }


        inline bool operator<(const J2KPacketFragment &f1, const J2KPacketFragment &f2)
        {
            return f1.id() < f2.id();
        }

        inline bool operator==(const J2KPacketFragment &f1, const J2KPacketFragment &f2)
        {
            return f1.id() == f2.id();
        }

    }
}



#endif
