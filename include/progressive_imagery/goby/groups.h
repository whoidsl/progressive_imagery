/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef PROGRESSIVE_IMAGERY_GROUPS_20180809H
#define PROGRESSIVE_IMAGERY_GROUPS_20180809H

#include "goby/middleware/group.h"


namespace dsl
{
    namespace progressive_imagery
    {
        namespace groups
        {
            constexpr goby::Group image_fragments { "dsl::progressive_imagery::image_fragments" };
            constexpr goby::Group ack_and_queue_update { "dsl::progressive_imagery::ack_and_queue_update" };
            
                
            constexpr goby::Group rx_status { "dsl::progressive_imagery::rx_status" };
            constexpr goby::Group queue_action { "dsl::progressive_imagery::queue_action" };
            constexpr goby::Group received_status{ "dsl::progressive_imagery::received_status" };
            constexpr goby::Group updated_image{ "dsl::progressive_imagery::updated_image" };
            constexpr goby::Group transfer_progress{ "dsl::progressive_imagery::transfer_progress" };

            constexpr goby::Group encode_report { "dsl::progressive_imagery::encode_report" };
            
        }
    }
}


#endif
